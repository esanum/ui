export enum PagerMode {
  page = 'page',
  offset = 'offset'
}

export enum PagerAction {
  page = 'page',
  start = 'start',
  back = 'back',
  next = 'next',
  end = 'end'
}
