import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'formatLinkPipe'})
export class FormatLinkPipe implements PipeTransform {
  transform(format: string, page: number): string {
    return format.replace('%page%', `${page}`);
  }
}

@Pipe({name: 'getAttributes'})
export class GetAttributesPipe implements PipeTransform {
  transform(page: string | number, callback: (page: string) => { [key: string]: string }): { [key: string]: string } {
    return !!callback ? callback(page.toString()) : {};
  }
}
