import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BindingModule } from '../../core/directives/binding.module';
import { IconModule } from '../../elements/icon/icon.module';
import { SelectModule } from '../../forms/select/select.module';
import { StackModule } from '../../layout/stack/stack.module';
import { PagerComponent } from './pager.component';
import { FormatLinkPipe, GetAttributesPipe } from './pipes';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IconModule,
    StackModule,
    SelectModule,
    BindingModule
  ],
  declarations: [
    PagerComponent,
    FormatLinkPipe,
    GetAttributesPipe
  ],
  entryComponents: [
    PagerComponent
  ],
  exports: [
    PagerComponent
  ]
})
export class PagerModule {
}
