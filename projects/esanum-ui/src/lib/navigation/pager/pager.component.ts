import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  Output
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { PagerAction, PagerMode } from './enums';

export const DEFAULT_PAGE_SIZE = 10;
export const DEFAULT_PAGE = 1;
export const DEFAULT_PAGER_SIZE = 3;

@Component({
  selector: 'sn-pager',
  templateUrl: './pager.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PagerComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PagerComponent implements ControlValueAccessor {

  ui = UI;

  @HostBinding('attr.pg')
  readonly host = '';

  pages: number[];
  pagerAction = PagerAction;

  private _count: number;
  private _pageSize = DEFAULT_PAGE_SIZE;
  private _selectedPage = DEFAULT_PAGE;
  private _size = DEFAULT_PAGER_SIZE;

  set selectedPage(page: number) {
    this._selectedPage = page;
    this.render();
  }

  get selectedPage() {
    return this._selectedPage;
  }

  @Input()
  set size(size: number) {
    this._size = size;
    this.render();
  }

  get size() {
    return this._size;
  }

  @Input()
  set count(count: number) {
    this._count = count;
    this.render();
  }

  get pagesCount() {
    return Math.ceil(this._count / this.pageSize);
  }

  @Input()
  set pageSize(pageSize: number) {
    this._pageSize = pageSize;
    this.render();
  }

  get pageSize() {
    return this._pageSize;
  }

  @Input()
  mode: PagerMode = PagerMode.offset;

  @Input()
  link: string = '?page=%page%';

  @Input()
  attributes: (page: string) => { [key: string]: string };

  @Output()
  action = new EventEmitter<PagerAction>();

  @HostBinding('style.visibility')
  get visible() {
    return this.pagesCount > 1 ? 'visible' : 'collapse';
  }

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;

  @HostListener('blur')
  onBlur = () => this.onTouched();

  constructor(private logger: NGXLogger,
              private cd: ChangeDetectorRef) {
  }

  writeValue(value: number): void {
    switch (this.mode) {
      case PagerMode.page:
        this.logger.debug('set page ', value);
        this.selectedPage = value;
        break;
      case PagerMode.offset:
        const page = Math.ceil(value / this.pageSize) + 1;
        this.logger.debug('set page ', page);
        this.selectedPage = page;
        break;
    }
  }

  setPage(page: number) {
    if (page >= DEFAULT_PAGE && page <= this.pagesCount) {
      this.onChange(this.mode === PagerMode.offset ? (page - 1) * this.pageSize : page);
      this.selectedPage = page;
    }
  }

  render() {
    const pages: number[] = [];

    let shift = Math.max(this.size - this.selectedPage + 1, 0);
    const end = Math.min(this.selectedPage + this.size + shift, this.pagesCount);

    shift = Math.max(this.selectedPage + this.size - this.pagesCount, 0);
    const start = Math.max(this.selectedPage - this.size - shift, 1);

    for (let i = start; i <= end; i++) {
      pages.push(i);
    }
    this.pages = pages;

    this.cd.detectChanges();
  }
}
