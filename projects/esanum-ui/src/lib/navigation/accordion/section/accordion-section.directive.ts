import { ContentChild, Directive, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { State } from '../../../core/enums/state';
import { UI } from '../../../core/enums/ui';

@Directive({
  selector: 'sn-accordion-section'
})
export class AccordionSectionDirective {

  ui = UI;

  private _title: string;
  private _icon: string;
  private _state: State;
  private _accordionContentTemplate: TemplateRef<any>;
  private _accordionTitleTemplate: TemplateRef<any>;

  @Input()
  set title(title: string) {
    this._title = title;
    this.updated.emit();
  }

  get title(): string {
    return this._title;
  }

  @Input()
  set icon(icon: string) {
    this._icon = icon;
    this.updated.emit();
  }

  get icon(): string {
    return this._icon;
  }

  @Input()
  set state(state: State) {
    this._state = state;
    this.updated.emit();
  }

  get state(): State {
    return this._state;
  }

  @Output()
  updated = new EventEmitter<any>();

  @ContentChild('accordionTitleTemplate')
  set accordionTitleTemplate(template: TemplateRef<any>) {
    this._accordionTitleTemplate = template;
    this.updated.emit();
  }

  get accordionTitleTemplate(): TemplateRef<any> {
    return this._accordionTitleTemplate;
  }

  @ContentChild('accordionContentTemplate')
  set accordionContentTemplate(template: TemplateRef<any>) {
    this._accordionContentTemplate = template;
    this.updated.emit();
  }

  get accordionContentTemplate(): TemplateRef<any> {
    return this._accordionContentTemplate;
  }
}
