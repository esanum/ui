import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  Output,
  QueryList
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { Behaviour } from '../../core/enums/behaviour';
import { Outline } from '../../core/enums/outline';
import { UI } from '../../core/enums/ui';
import { AccordionSectionDirective } from './section/accordion-section.directive';

enum AnimationState {
  default = 'default',
  opened = 'opened',
  closed = 'closed'
}

@Component({
  selector: 'sn-accordion',
  templateUrl: './accordion.component.html',
  animations: [
    trigger('rotate', [
        state('open', style({transform: 'rotate(-180deg)'})),
        state('close', style({transform: 'rotate(0deg)'})),
        transition('open <=> close', [animate('.3s ease-in-out')])
      ]
    ),
    trigger('collapse', [
        transition(`* => ${AnimationState.default}`, []),
        transition(':enter', [style({height: 0}), animate('.3s', style({height: '*'}))]),
        transition(':leave', [style({height: '*'}), animate('.3s', style({height: 0}))])
      ]
    )
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccordionComponent implements AfterViewInit, OnDestroy {

  ui = UI;

  @HostBinding('attr.an')
  readonly host = '';

  @HostBinding('attr.data-sn-outline')
  _outline: Outline = Outline.ghost;

  private _behaviour: Behaviour = Behaviour.single;

  private _subscriptions: { sections: Subscription } = {sections: null};

  animate = AnimationState.default;

  @Input()
  active: number[] = [];

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.ghost;
  }

  get outline() {
    return this._outline;
  }

  @Input()
  set behaviour(behaviour: Behaviour) {
    this._behaviour = behaviour || Behaviour.single;
  }

  get behaviour() {
    return this._behaviour;
  }

  @Output()
  changed = new EventEmitter<number[]>();

  @ContentChildren(AccordionSectionDirective)
  sections: QueryList<AccordionSectionDirective>;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.listenSections();
    this.sections.changes
      .subscribe(() => {
        this.cd.detectChanges();
        this.listenSections();
      });
  }

  ngOnDestroy() {
    this._subscriptions.sections?.unsubscribe();
  }

  private listenSections() {
    this._subscriptions.sections?.unsubscribe();
    this._subscriptions.sections = merge(...this.sections.map(element => element.updated))
      .subscribe(() => this.cd.detectChanges());
  }

  setActive(index: number, event: Event) {
    if (this.active.includes(index)) {
      this.active.splice(this.active.indexOf(index), 1);
    } else {
      if (this.behaviour === Behaviour.single) {
        this.active.splice(0, this.active.length, index);
      } else {
        this.active.push(index);
      }
    }
    this.changed.emit(this.active);
    event.preventDefault();
  }

}
