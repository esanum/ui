import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TextPipesModule } from '../../core/pipes/text-pipes.module';
import { ArrayPipesModule } from '../../core/pipes/array-pipes.module';
import { IconModule } from '../../elements/icon/icon.module';
import { SpinnerModule } from '../../layout/spinner/spinner.module';
import { StackModule } from '../../layout/stack/stack.module';
import { AccordionComponent } from './accordion.component';
import { AccordionSectionDirective } from './section/accordion-section.directive';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    StackModule,
    SpinnerModule,
    ArrayPipesModule,
    TextPipesModule
  ],
  declarations: [
    AccordionComponent,
    AccordionSectionDirective
  ],
  exports: [
    AccordionComponent,
    AccordionSectionDirective
  ]
})
export class AccordionModule {
}
