import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

type LinkOptions = {
  hash: string;
}

@Directive({
  selector: '[spaLink]'
})
export class SpaLinkDirective {

  @Input()
  set spaLink(spaLink: string | Partial<LinkOptions>) {
    if (!!spaLink) {
      let url = typeof spaLink === 'object'
        ? Object.assign(new URL(location.href), spaLink).toString()
        : spaLink;

      const host = this.hostRef.nativeElement;
      if (!!url) {
        this.renderer.setAttribute(host, 'href', url)
      } else {
        this.renderer.removeAttribute(host, 'href');
      }
    }
  }

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent) {
    event.preventDefault();
  }

  constructor(private hostRef: ElementRef,
              private renderer: Renderer2) {
  }
}
