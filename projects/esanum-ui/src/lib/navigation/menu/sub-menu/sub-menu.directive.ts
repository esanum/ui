import { ContentChildren, Directive, Input, QueryList } from '@angular/core';
import { Feature } from '../../../core/enums/feature';
import { SubMenuItemDirective } from './sub-menu-item.directive';

@Directive({
  selector: 'sn-sub-menu'
})
export class SubMenuDirective {

  @Input()
  features: Feature[] = [Feature.title];

  @ContentChildren(SubMenuItemDirective)
  items: QueryList<SubMenuItemDirective>;
}
