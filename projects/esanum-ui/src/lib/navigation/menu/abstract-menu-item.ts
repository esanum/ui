import { ContentChildren, Directive, EventEmitter, HostBinding, Input, Output, QueryList } from '@angular/core';
import { Feature } from '../../core/enums/feature';
import { UI } from '../../core/enums/ui';
import { UrlMatching } from '../../core/enums/url';
import { BadgeComponent } from '../../elements/badge/badge.component';
import { LinkTarget } from '../link/link.enums';

@Directive()
export abstract class AbstractMenuItem {

  ui = UI;

  _matching: UrlMatching = UrlMatching.fullMatch;

  @HostBinding('attr.opened')
  opened = false;

  @Input()
  loading = false;

  @Input()
  icon: string;

  @Input()
  disabled = false;

  @Input()
  title: string;

  @Input()
  link: string | (string | { [key: string]: string | number })[];

  @Input()
  state: { [k: string]: any };

  @Input()
  target: string = LinkTarget.self;

  @Input()
  matching: UrlMatching = UrlMatching.fullMatch;

  @Input()
  active = false;

  @Input()
  queryParams: { [k: string]: any };

  @Input()
  fragment: string;

  @Input()
  features: Feature[] = [];

  @Output()
  click = new EventEmitter<any>();

  @Input()
  attributes: { [key: string]: string };

  @ContentChildren(BadgeComponent)
  badges: QueryList<BadgeComponent>;

}
