import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BindingModule } from '../../core/directives/binding.module';
import { ArrayPipesModule } from '../../core/pipes/array-pipes.module';
import { SpinnerModule } from '../../layout/spinner/spinner.module';
import { StackModule } from '../../layout/stack/stack.module';
import { IconModule } from '../icon/icon.module';
import { PictureComponent } from './picture.component';

@NgModule({
    imports: [
        CommonModule,
        IconModule,
        StackModule,
        BindingModule,
        SpinnerModule,
        ArrayPipesModule
    ],
  exports: [
    PictureComponent
  ],
  entryComponents: [
    PictureComponent
  ],
  declarations: [
    PictureComponent
  ]
})
export class PictureModule {
}
