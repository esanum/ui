import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  HostBinding,
  Input,
  Renderer2,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import ResizeObserver from 'resize-observer-polyfill';
import { Feature } from '../../core/enums/feature';
import { Fit } from '../../core/enums/fit';
import { Position } from '../../core/enums/position';
import { UI } from '../../core/enums/ui';

const CHECK_INTERVAL = 300;

enum Area {
  leftTop = 'leftTop',
  rightTop = 'rightTop',
  leftBottom = 'leftBottom',
  rightBottom = 'rightBottom',
}

interface Image {
  width: number;
  height: number;
  x: number;
  y: number;
}

interface GravityPoint {
  x: number;
  y: number;
}

interface Sizes {
  width: number;
  height: number;
  url: string;
}

@Component({
  selector: 'sn-picture',
  templateUrl: './picture.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PictureComponent {

  @HostBinding('attr.pi')
  readonly host = '';

  ui = UI;
  feature = Feature;

  _src: string | SafeUrl;
  _icon = UI.icons.image;

  private _pictureCopyrightTemplate: TemplateRef<any>;
  private _gravityPoint: GravityPoint = null;
  private _sizes: Sizes[] = [];

  observers = {
    gravity: new ResizeObserver(() => this.render()),
    fit: new ResizeObserver(() => this.setFit())
  };

  picture: Image;
  image: Image;
  area: Area;
  url: string;
  x = 0;
  y = 0;

  @HostBinding('attr.data-sn-has-src')
  get hasSrc() {
    return !!this._src;
  }

  @HostBinding('attr.data-sn-has-url')
  get hasUrl() {
    return !!this.url;
  }

  @HostBinding('attr.data-has-gravity')
  get hasGravityPoint() {
    return !!this._gravityPoint;
  }

  @HostBinding('attr.data-sn-fit')
  _fit: Fit = Fit.width;

  @HostBinding('attr.data-sn-position')
  _position: Position = Position.center;

  @Input()
  set icon(icon: string) {
    this._icon = icon || UI.icons.image;
  }

  get icon() {
    return this._icon;
  }

  @Input()
  set src(src: string | SafeUrl) {
    this._src = src || null;
  }

  get src() {
    return this._src;
  }

  @HostBinding('attr.title')
  @Input()
  title: string;

  @HostBinding('attr.alt')
  @Input()
  alt: string;

  @HostBinding('style.width')
  @Input()
  width: string;

  @HostBinding('style.height')
  @Input()
  height: string;

  @Input()
  set fit(fit: Fit) {
    const host = this.hostRef?.nativeElement;
    if (fit === Fit.auto) {
      this.observers.fit.observe(host);
    } else {
      this.observers.fit?.unobserve(host);
      this._fit = fit || Fit.width;
    }
  }

  @Input()
  set position(position: Position) {
    this._position = position || Position.center;
  }

  @Input()
  set gravityPoint(gravityPoint: GravityPoint) {
    this._gravityPoint = gravityPoint;

    const host = this.hostRef?.nativeElement;
    if (!!gravityPoint) {
      this.observers.gravity?.observe(host);
    } else {
      this.clear();
      this.observers.gravity?.unobserve(host);
    }
  }

  get gravityPoint() {
    return this._gravityPoint;
  }

  @Input()
  set sizes(sizes: Sizes[]) {
    this._sizes = sizes;
    if (sizes.length > 0 && !!this.gravityPoint) {
      this.render();
    } else {
      this.clear();
    }
  }

  get sizes() {
    return this._sizes;
  }

  @Input()
  loading: boolean;

  @HostBinding('attr.data-features')
  @Input()
  features: Feature[] = [];

  @Input()
  attributes: { [key: string]: string };

  @ContentChild('pictureCopyrightTemplate')
  set pictureCopyrightTemplate(templateRef: TemplateRef<any>) {
    this._pictureCopyrightTemplate = templateRef;
    this.cd.detectChanges();
  }

  get pictureCopyrightTemplate() {
    return this._pictureCopyrightTemplate;
  }

  @ViewChild('imageRef')
  imageRef: ElementRef;

  constructor(private cd: ChangeDetectorRef,
              private hostRef: ElementRef,
              private renderer: Renderer2) {
  }

  ngOnDestroy() {
    this.observers.gravity?.unobserve(this.hostRef?.nativeElement);
    this.observers.fit?.unobserve(this.hostRef?.nativeElement);
  }

  setFit() {
    let check: () => void;
    check = () => {
      const host = this.hostRef?.nativeElement;
      const img = this.imageRef?.nativeElement;

      const sizes = {
        host: {
          width: host?.getBoundingClientRect()?.width,
          height: host?.getBoundingClientRect()?.height
        },
        img: {
          width: img?.getBoundingClientRect()?.width,
          height: img?.getBoundingClientRect()?.height
        }
      }

      if (!!img && !!host && !!sizes.img.width && !!sizes.img.height) {
        if (sizes.host.width > sizes.img.width) {
          this._fit = Fit.width;
        } else {
          if (sizes.host.height > sizes.img.height) {
            this._fit = Fit.height;
          }
        }
        this.renderer.setAttribute(this.hostRef.nativeElement, 'data-sn-fit', this._fit);
      } else {
        setTimeout(() => check(), CHECK_INTERVAL);
      }
      this.cd.detectChanges();
    };
    check();
  }

  render() {
    this.setSizes();
    if (!!this.image) {
      this.area = this.getArea();
      this.transform();
      this.cd.detectChanges();
    }
  }

  clear() {
    this.x = 0;
    this.y = 0;
    this.url = null;
  }

  setSizes() {
    const picture = this.hostRef?.nativeElement.getBoundingClientRect();
    this.picture = {
      width: picture.width,
      height: picture.height,
      x: (picture.width * 50) / 100,
      y: (picture.height * 50) / 100
    };

    const size = this.sizes
      .sort((a, b) => a.width - b.width)
      .find(size => size.width >= this.picture.width);

    if (!!size) {
      this.url = size.url;
      this.image = {
        width: size.width,
        height: size.height,
        x: (size.width * this.gravityPoint.x) / 100,
        y: (size.height * this.gravityPoint.y) / 100
      };
    } else {
      this.url = null;
      this.image = null;
    }
  }

  getArea() {
    if (this.picture.x < this.image.x) {
      if (this.picture.y < this.image.y) {
        return Area.rightBottom;
      }
      return Area.rightTop;
    }

    if (this.picture.y < this.image.y) {
      return Area.leftBottom;
    }

    return Area.leftTop;
  }

  transform() {
    const shiftX = this.picture.x - this.image.x;
    const shiftY = this.picture.y - this.image.y;

    const shiftW = this.picture.width - this.image.width;
    const shiftH = this.picture.height - this.image.height;

    const overflowX = this.picture.x > this.image.width - this.image.x;
    const overflowY = this.picture.y > this.image.height - this.image.y;

    this.x = 0;
    this.y = 0;

    switch (this.area) {
      case Area.rightBottom:
        this.x = overflowX ? shiftW : shiftX;
        this.y = overflowY ? shiftH : shiftY;
        break;

      case Area.rightTop:
        this.x = overflowX ? shiftW : shiftX;
        break;

      case Area.leftBottom:
        this.y = overflowY ? shiftH : shiftY;
        break;
    }
  }
}
