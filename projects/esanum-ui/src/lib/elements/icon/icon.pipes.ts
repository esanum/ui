import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'inPixels'})
export class InPixelsPipe implements PipeTransform {
  transform(size: string): boolean {
    return /^\d+px$/.test(size);
  }
}
