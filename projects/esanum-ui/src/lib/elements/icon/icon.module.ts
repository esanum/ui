import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AnimatedIconComponent } from './animated/animated-icon.component';
import { IconComponent } from './icon.component';
import { InPixelsPipe } from './icon.pipes';
import { SvgIconComponent } from './svg/svg-icon.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    IconComponent,
    AnimatedIconComponent,
    SvgIconComponent,
    InPixelsPipe
  ],
  entryComponents: [
    IconComponent
  ],
  exports: [
    IconComponent
  ]
})
export class IconModule {

}
