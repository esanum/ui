import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, OnInit, Renderer2 } from '@angular/core';
import { map } from 'rxjs/operators';
import { EsanumUIConfig } from '../../../config';
import { InMemoryCacheService } from '../../../core/services/in-memory-cache.service';
import { IconTag } from '../enums';

const DEFAULT_ICONSET = 'default';

@Component({
  selector: 'sn-animated-icon',
  templateUrl: './animated-icon.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnimatedIconComponent implements OnInit {

  @HostBinding('attr.ai')
  readonly host = '';

  private _initialized = false;
  private svg: HTMLElement;
  private _color: string;
  private _iconset = DEFAULT_ICONSET;
  private _icon: string;
  private _size: string;
  private _tags: string[];

  @Input()
  set iconset(iconset: string) {
    this._iconset = iconset;
    if (this._initialized) {
      this.load();
    }
  }

  get iconset() {
    return this._iconset;
  }

  @HostBinding('attr.data-sn-icon')
  @Input()
  set icon(icon: string) {
    this._icon = icon;
    if (this._initialized) {
      this.load();
    }
  }

  get icon() {
    return this._icon;
  }

  @Input()
  set size(size: string) {
    this._size = size;
    if (!!this.svg) {
      this.render();
    }
  }

  get size() {
    return this._size;
  }

  @Input()
  set color(color: string) {
    this._color = color;
    if (!!this.svg) {
      this.render();
    }
  }

  get color() {
    return this._color;
  }

  @HostBinding('attr.data-sn-has-color')
  get hasColor() {
    return !!this.color;
  }

  @Input()
  @HostBinding('attr.data-sn-tags')
  set tags(tags: string[]) {
    this._tags = tags;
    if (!!this.svg) {
      this.render();
    }
  }

  get tags() {
    return this._tags;
  }

  constructor(private http: HttpClient,
              private renderer: Renderer2,
              private cache: InMemoryCacheService,
              private hostRef: ElementRef,
              private config: EsanumUIConfig) {
  }

  ngOnInit() {
    this._initialized = true;
    this.load();
  }

  render() {
    this.svg.setAttribute('fill', 'none');
    this.svg.setAttribute('stroke', 'none');
    this.svg.setAttribute('width', '100%');
    this.svg.setAttribute('height', '100%');

    if (!!this.size) {
      this.svg.setAttribute('width', this.size);
      this.svg.setAttribute('height', this.size);
    }
    if (!!this.color && this.tags.length > 0) {
      if (this.tags.includes(IconTag.stroked)) {
        this.svg.setAttribute('stroke', this.color);
      }
      if (this.tags.includes(IconTag.filled)) {
        this.svg.setAttribute('fill', this.color);
      }
    }

    const el = this.hostRef.nativeElement;
    this.renderer.setProperty(el, 'innerHTML', this.svg.outerHTML);
  }

  private load() {
    if (!this.iconset || !this.icon) {
      return;
    }

    const path = `${this.config.assets}/icons/animated/${this.iconset}/${this.icon}.svg?hash=${this.config.hash}`;

    let icon = this.cache.get<HTMLElement>(path);
    if (icon === undefined) {
      this.http.get(path, {responseType: 'text'})
        .pipe(map(resp => new DOMParser().parseFromString(resp, 'application/xml')))
        .subscribe(file => {
          icon = file.documentElement;
          const encapsulate = (el: Element) => {
            el.setAttribute('_ai', this.host);
            for (let i = 0; i < el.children.length; i++) {
              encapsulate(el.children[i]);
            }
          };

          encapsulate(icon);
          icon.setAttribute('width', '100%');
          icon.setAttribute('height', '100%');
          this.svg = icon;
          this.render();

          this.cache.set(path, icon);
        });
    } else {
      this.svg = icon;
      this.render();
    }
  }

}
