import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  HostBinding,
  Input, Output
} from '@angular/core';
import { Color } from '../../core/enums/color';
import { Feature } from '../../core/enums/feature';

@Component({
  selector: 'sn-dot',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DotComponent {

  @HostBinding('attr.dt')
  readonly host = '';

  @HostBinding('style.background-color')
  @HostBinding('attr.data-sn-color')
  _color: string = Color.primary;

  @HostBinding('attr.data-sn-features')
  _features: Feature[] = [];

  @Input()
  set color(color: string) {
    this._color = color || Color.primary;
    this.updated.emit();
  }

  get color() {
    return this._color;
  }

  @Input()
  set features(features: Feature[]) {
    this._features = features;
    this.updated.emit();
  }

  get features() {
    return this._features;
  }

  @Output()
  updated = new EventEmitter<any>();
}
