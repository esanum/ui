import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  HostBinding,
  Input,
  OnDestroy,
  QueryList
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { AvatarComponent } from '../avatar.component';

@Component({
  selector: 'sn-avatars-list',
  templateUrl: './avatars-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarsListComponent implements AfterViewInit, OnDestroy {

  @HostBinding('attr.at')
  readonly host = '';

  ui = UI;

  private _subscriptions: { avatars: Subscription } = {avatars: null};

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @Input()
  max = 5;

  @ContentChildren(AvatarComponent)
  avatars: QueryList<AvatarComponent>;

  @HostBinding('attr.data-sn-capacity')
  get capacity() {
    return Math.min(this.avatars.length, this.max);
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.listenAvatars();
    this.avatars.changes.subscribe(() => {
      this.listenAvatars();
      this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this._subscriptions.avatars?.unsubscribe();
  }

  private listenAvatars() {
    this._subscriptions.avatars?.unsubscribe();
    this._subscriptions.avatars = merge(...this.avatars.map(avatar => avatar.updated))
      .subscribe(() => this.cd.detectChanges());
  }
}
