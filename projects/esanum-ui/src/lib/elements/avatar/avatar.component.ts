import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';
import { Shape } from '../../core/enums/shape';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { DotComponent } from '../dot/dot.component';

@Component({
  selector: 'sn-avatar',
  templateUrl: './avatar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent {

  @HostBinding('attr.av')
  readonly host = '';

  ui = UI;

  private _icon: string;
  private _name: string;
  private _surname: string;
  private _image: string;
  private _dot: DotComponent;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-shape')
  _shape: Shape = Shape.circle;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set shape(shape: Shape) {
    this._shape = shape || Shape.circle;
  }

  @Input()
  set icon(icon: string) {
    this._icon = icon || UI.icons.user;
    this.updated.emit();
  }

  get icon(): string {
    return this._icon;
  }

  @Input()
  set name(name: string) {
    this._name = name;
    this.updated.emit();
  }

  get name(): string {
    return this._name;
  }

  @Input()
  set surname(surname: string) {
    this._surname = surname;
    this.updated.emit();
  }

  get surname(): string {
    return this._surname;
  }

  @Input()
  set image(image: string) {
    this._image = image;
    this.updated.emit();
  }

  get image(): string {
    return this._image;
  }

  @Input()
  attributes: { [key: string]: string };

  @Output()
  updated = new EventEmitter<any>();

  @ContentChild(DotComponent)
  set dot(dot: DotComponent) {
    this._dot = dot;
    this.cd.detectChanges();
  }

  get dot(): DotComponent {
    return this._dot;
  }

  constructor(private cd: ChangeDetectorRef) {
  }
}
