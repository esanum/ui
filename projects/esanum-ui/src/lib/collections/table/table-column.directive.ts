import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { TextAlign } from '../../core/enums/text';

@Directive({
  selector: 'sn-table-column'
})
export class TableColumnDirective {

  @ContentChild('tableCellTemplate')
  tableCellTemplate: TemplateRef<any>;

  @Input()
  title: string;

  @Input()
  width: string;

  @Input()
  align: TextAlign = TextAlign.left;

  @Input()
  orderBy: string;
}
