import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';

@Directive({
  selector: 'sn-gantt-line-period'
})
export class GanttLinePeriodDirective {

  @Input()
  from: Date;

  @Input()
  to: Date;

  @ContentChild('indicatorMonthTemplate')
  indicatorMonthTemplate: TemplateRef<any>;

  @ContentChild('indicatorYearTemplate')
  indicatorYearTemplate: TemplateRef<any>;
}
