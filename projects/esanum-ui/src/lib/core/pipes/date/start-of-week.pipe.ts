import { Pipe, PipeTransform } from '@angular/core';
import startOfWeek from 'date-fns/startOfWeek';
import { EsanumUIConfig } from '../../../config';

@Pipe({name: 'snStartOfWeek'})
export class StartOfWeekPipe implements PipeTransform {

  constructor(private config: EsanumUIConfig) {
  }

  transform(date: Date): Date {
    return startOfWeek(date, {
      locale: this.config.locale.dfns,
      weekStartsOn: this.config.weekStartsOn
    });
  }
}
