import { Pipe, PipeTransform, } from '@angular/core';
import formatDistance from 'date-fns/formatDistance';
import { EsanumUIConfig } from '../../../config';

@Pipe({name: 'snFormatDistance'})
export class FormatDistancePipe implements PipeTransform {

  constructor(private config: EsanumUIConfig) {
  }

  transform(start: Date, end: Date): string {
    return formatDistance(start, end, {
      includeSeconds: false,
      addSuffix: false,
      locale: this.config.locale.dfns
    });
  }
}
