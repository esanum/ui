export enum Context {
  block = 'block',
  inline = 'inline',
  modal = 'modal',
  text = 'text',
  box = 'box',
  menu = 'menu',
  popover = 'popover'
}
