export enum MenuStyle {
  default = 'default',
  tabs = 'tabs',
  tags = 'tags'
}

export enum SwitchStyle {
  default = 'default',
  hipster = 'hipster'
}

export enum ProgressBarStyle {
  default = 'default',
  filled = 'filled'
}
