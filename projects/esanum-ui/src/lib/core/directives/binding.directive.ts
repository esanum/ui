import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

export class BindingConfig {
  attributes: { [key: string]: string };

  constructor(defs: Partial<BindingConfig> = null) {
    Object.assign(this, defs);
  }
}

@Directive({
  selector: '[snBinding]',
  exportAs: 'snBinding'
})
export class BindingDirective {

  @Input('snBinding')
  set config(config: Partial<BindingConfig>) {
    Object.keys(config.attributes || {})
      .filter(key => config.attributes[key] !== null && config.attributes[key] !== undefined)
      .forEach(key => this.renderer.setAttribute(this.hostRef.nativeElement, key, config.attributes[key]));
  }

  constructor(private hostRef: ElementRef,
              private renderer: Renderer2) {
  }

}
