import { Directive, ElementRef, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';

@Directive({
  selector: '[snClipboard]'
})
export class ClipboardDirective implements OnInit {

  @Input('snClipboard')
  content: string;

  @Output()
  onSuccess = new EventEmitter();

  @Output()
  onError = new EventEmitter();

  constructor(private ngZone: NgZone,
              private hostRef: ElementRef) {
  }

  public ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      this.hostRef.nativeElement.addEventListener('click', () => {
        if (!!this.content) {
          navigator.clipboard.writeText(this.content)
            .then(() => this.onSuccess.emit())
            .catch(err => this.onError.emit(err));
        }
      });
    });
  }
}
