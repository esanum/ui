import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BindingDirective } from './binding.directive';

@NgModule({
  declarations: [BindingDirective],
  imports: [CommonModule],
  exports: [BindingDirective]
})
export class BindingModule {
}
