import { Directive, ElementRef, Input, NgZone, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { assign } from 'lodash';

export enum StickyDirection {
  top = 'top',
  bottom = 'bottom'
}

interface StickyOption {
  direction?: StickyDirection;
  parent?: ElementRef;
  topShift?: number;
  bottomShift?: number;
  disabled?: boolean;
  attribute?: string;
}

@Directive({
  selector: '[snSticky]'
})
export class StickyDirective implements OnInit, OnDestroy {

  private listeners: Function[] = [];

  options: StickyOption = {
    direction: StickyDirection.top,
    parent: null,
    topShift: 0,
    bottomShift: 0,
    disabled: false,
    attribute: 'data-position'
  };

  @Input('snSticky')
  set ___options__(options: Partial<StickyOption>) {
    assign(this.options, options);
  }

  get sticker() {
    return this.hostRef.nativeElement;
  }

  get position() {
    const parentRect = this.options.parent?.nativeElement.getBoundingClientRect();
    return parentRect.top + window.scrollY - this.options.topShift;
  }

  constructor(private hostRef: ElementRef,
              private renderer: Renderer2,
              private zone: NgZone) {
  }

  ngOnInit() {
    if (this.options.disabled) {
      return;
    }

    this.zone.runOutsideAngular(() => this.listeners.push(
      this.renderer.listen('document', 'scroll', () => this.documentScrolled())
    ));
  }

  ngOnDestroy() {
    this.listeners.forEach(listener => listener());
  }

  private documentScrolled() {
    if (this.options.direction === StickyDirection.top) {
      this.stickTop();
    } else {
      this.stickBottom();
    }
  }

  private stickTop() {
    const {attribute} = this.options;

    if (window.scrollY >= this.position) {
      this.renderer.setAttribute(this.sticker, attribute, 'middle');
      this.scrolledUnder();
    } else {
      this.renderer.removeAttribute(this.sticker, attribute);
      this.renderer.removeStyle(this.sticker, 'left');
    }
  }

  private scrolledUnder() {
    const {parent, bottomShift, attribute} = this.options;

    if (!!parent) {
      const parentRect = parent.nativeElement.getBoundingClientRect();
      let stickerRect = this.sticker.getBoundingClientRect();
      const position = parentRect.right - stickerRect.width - bottomShift;

      this.renderer.setStyle(this.sticker, 'left', position + 'px');
      stickerRect = this.sticker.getBoundingClientRect();

      if (parentRect.bottom - bottomShift <= stickerRect.bottom) {
        this.renderer.setAttribute(this.sticker, attribute, 'bottom');
        this.renderer.removeStyle(this.sticker, 'left');
      }
    }
  }

  private stickBottom() {
    const {parent, attribute} = this.options;

    if (!!parent) {
      const parentRect = parent.nativeElement.getBoundingClientRect();
      let stickerRect = this.sticker.getBoundingClientRect();

      if (parentRect.bottom <= stickerRect.bottom) {
        this.renderer.setAttribute(this.sticker, attribute, 'bottom');
        stickerRect = this.sticker.getBoundingClientRect();
      }
      if (document.documentElement.clientHeight <= stickerRect.bottom) {
        this.renderer.removeAttribute(this.sticker, attribute);
      }
    }
  }

}
