import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { BadgeComponent } from '../../elements/badge/badge.component';
import { DotComponent } from '../../elements/dot/dot.component';

@Directive({
  selector: 'sn-switcher-option'
})
export class SwitcherOptionDirective {

  @Input()
  label: string;

  @Input()
  value: any;

  @Input()
  icon: string;

  @Input()
  disabled: boolean;

  active = false;

  @ContentChild(DotComponent)
  dot: DotComponent;

  @ContentChild(BadgeComponent)
  badge: BadgeComponent;

  @ContentChild('optionTemplate')
  optionTemplate: TemplateRef<any>;

}
