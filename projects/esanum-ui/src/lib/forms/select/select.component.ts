import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Behaviour } from '../../core/enums/behaviour';
import { Breakpoint } from '../../core/enums/breakpoint';
import { Context } from '../../core/enums/context';
import { Feature } from '../../core/enums/feature';
import { Key as Keyboard } from '../../core/enums/keyboard';
import { Placement } from '../../core/enums/placement';
import { Size } from '../../core/enums/size';
import { State } from '../../core/enums/state';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { progress } from '../../core/utils/rxjs';
import { BreakpointService } from '../../layout/responsive/breakpoint.service';
import { DeviceService } from '../../layout/responsive/device.service';
import { PopoverInstance, PopoverService } from '../../overlays/popover/popover.service';
import { InputAutocomplete } from '../input/input.enums';
import { SelectMode } from './enums';
import { IOption, Key, Options } from './model';

@Directive({
  selector: 'sn-select-option'
})
export class SelectOptionDirective {

  ui = UI;

  @Input()
  icon: string;

  @Input()
  key: Key;

  @Input()
  label: string;

  @Input()
  value: any;
}

const CHECKING_INTERVAL = 100;
const SEARCH_DELAY = 100;

@Component({
  selector: 'sn-select',
  templateUrl: './select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent implements OnInit, AfterContentInit, OnDestroy, ControlValueAccessor {

  @HostBinding('attr.se')
  readonly host = '';

  ui = UI;

  private reference: { popover: PopoverInstance } = {popover: null};
  private destroyed = new Subject();
  private _features: Feature[] = [];
  private fetcher: Subscription;
  private _placement: Placement = Placement.absolute;

  options: Options = {persisted: {}, found: {}};
  changes = {selected: 0, options: 0};
  selected: Key[] = [];
  progress = {options$: new Subject<boolean>()};

  queryControl = this.fb.control({value: null, disabled: true});
  form = this.fb.group(
    {
      query: this.queryControl
    }
  );

  @Input()
  labelField = 'label';

  @Input()
  keyField = 'key';

  @Input()
  groupField = null;

  @Input()
  groupFieldKey = null;

  @Input()
  placeholder = '';

  @Input()
  required = false;

  @Input()
  optionTemplate: TemplateRef<any>;

  @Input()
  emptyOptionsTemplate: TemplateRef<any>;

  @Input()
  optionsHeaderTemplate: TemplateRef<any>;

  @Input()
  label: string;

  @Input()
  icon: string;

  @Input()
  groupTemplate: TemplateRef<any>;

  @Input()
  autocomplete: InputAutocomplete = InputAutocomplete.off;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
  }

  @Input()
  set placement(placement: Placement) {
    this._placement = placement || Placement.absolute;
  }

  get placement() {
    return this._placement;
  }

  @Input()
  set mode(mode: SelectMode) {
    this._mode = mode || SelectMode.single;
  }

  get mode() {
    return this._mode;
  }

  @Input()
  loader: (query: string) => Observable<(Object & { icon: string })[]> = null;

  @Input()
  creator: (query: string, close: Function) => Observable<null> | null = null;

  @HostBinding('attr.data-sn-features')
  @Input()
  set features(features: Feature[]) {
    this._features = features || [];
    this.features.includes(Feature.search)
      ? this.queryControl.enable({emitEvent: false})
      : this.queryControl.disable({emitEvent: false});
  }

  get features() {
    return this._features;
  }

  @HostBinding('attr.data-context')
  @Input()
  context: Context;

  @HostBinding('attr.data-sn-state')
  @Input()
  state: State;

  @HostBinding('attr.data-sn-disabled')
  @Input()
  disabled = false;

  @HostBinding('attr.data-sn-empty')
  get empty() {
    return this.selected.length === 0;
  }

  @HostBinding('attr.data-inline')
  get inline() {
    return this.breakpoint.current === Breakpoint.mobile || this.context === UI.context.inline;
  }

  @HostBinding('attr.data-sn-mode')
  _mode: SelectMode = SelectMode.single;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @HostBinding('attr.data-sn-opened')
  opened = false;

  @ContentChildren(SelectOptionDirective)
  optionsFromMarkup: QueryList<SelectOptionDirective>;

  @ViewChild('optionsTemplate')
  optionsTemplate: TemplateRef<any>;

  @ViewChild('queryRef')
  queryRef: ElementRef<HTMLInputElement>;

  @ViewChild('selectedRef')
  selectedRef: ElementRef<HTMLUListElement>;

  @ViewChild('layoutRef', {read: ElementRef})
  layoutRef: ElementRef<HTMLElement>;

  @ViewChild('iconRef', {read: ElementRef, static: false})
  iconRef: ElementRef;

  @Output('selected')
  updated = new EventEmitter<any>();

  onChange: (value: Key | Key[]) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;

  constructor(private hostRef: ElementRef,
              private fb: FormBuilder,
              private popover: PopoverService,
              private logger: NGXLogger,
              private breakpoint: BreakpointService,
              public device: DeviceService,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.popover.attached
      .pipe(takeUntil((this.destroyed)),
        filter(t => this.opened && t !== this.hostRef)
      ).subscribe(() => this.close());

    const loadOptions = (query: string) => {
      if (!!this.fetcher) {
        this.fetcher.unsubscribe();
      }

      if (!!this.loader) {
        this.fetcher = this.loader(query)
          .pipe(progress(this.progress.options$))
          .subscribe(objects => {
            this.options.found = {};
            objects.forEach((o, index) => {
              const key = o[this.keyField];
              if (!!key) {
                this.options.found[`${key}`] = {
                  index,
                  key,
                  label: o[this.labelField],
                  icon: o.icon,
                  value: o
                };
              }
            });
            this.changes.options++;
          });
      } else {
        this.options.found = {};
        let options = Object.values(this.options.persisted);
        options = options.filter(o => o.label.toLocaleLowerCase()
          .includes(query.toLocaleLowerCase()));
        options.forEach(option => this.options.found[option.key] = option);
        this.changes.options++;
      }
      this.cd.detectChanges();
    };

    this.queryControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap(() => {
        this.logger.debug('query has been changed');
        if (!this.opened && !this.inline) {
          this.open();
        }
      }),
      debounceTime(SEARCH_DELAY),
      filter(query => !!query)
    ).subscribe(query => loadOptions(query));
  }

  ngAfterContentInit() {
    const convert = (options: SelectOptionDirective[]) => {
      this.options.persisted = {};
      options.forEach(({key, label, icon, value}, index) =>
        this.options.persisted[`${key}`] = {index, key, label, icon, value});
      this.changes.options++;
    };

    convert(this.optionsFromMarkup.toArray());
    this.optionsFromMarkup.changes
      .pipe(tap(() => this.logger.debug('options from markup changed')))
      .subscribe(options => convert(options.toArray()));
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
  }

  @HostListener('document:click', ['$event'])
  onClickOutside(event: Event) {
    const path = event.composedPath();
    const picked = path.indexOf(this.hostRef.nativeElement) !== -1;
    if (!!this.reference.popover && this.opened && !picked && !this.reference.popover.picked(path)) {
      this.close();
    }
  }

  @HostListener('click', ['$event'])
  onFocus({target}: { target: HTMLElement, path: HTMLElement[] }) {
    switch (this.mode) {
      case SelectMode.single:
        break;
      case SelectMode.multiple:
        if (target === this.selectedRef.nativeElement) {
          this.opened ? this.close() : this.open();
        }
        break;
    }
  }

  @HostListener('blur')
  onBlur() {
    this.onTouched();
  }

  @HostListener('click', ['$event'])
  onClick({target}: { target: HTMLElement }) {
    // TODO: think about iconRef
    const elements = [
      this.layoutRef.nativeElement,
      this.selectedRef.nativeElement,
      this.iconRef?.nativeElement
    ];
    if (elements.includes(target)) {
      this.open();
    }
  }

  trackOption(index: number, option: IOption) {
    return option.key || index;
  }

  toggle(option: IOption) {
    this.selected.indexOf(option.key) === -1
      ? this.select(option)
      : this.remove(option.key);
  }

  select(option: IOption) {
    this.logger.debug('option is selected');
    this.options.persisted[`${option.key}`] = option;
    this.changes.options++;
    if (this.mode === SelectMode.multiple) {
      this.selected.push(option.key);
      if (!!this.reference.popover) {
        this.reference.popover.update();
      }
    } else {
      this.selected = [option.key];
    }

    if (this.mode !== SelectMode.multiple || !this.features.includes(Feature.multiplex)) {
      this.close();
    }
    this.onChange(this.mode === SelectMode.multiple ? this.selected : option.key);
    this.updated.emit(option.value);
  }

  remove(key: Key) {
    const index = this.selected.findIndex(i => i === key);
    if (index !== -1 && (this.mode === SelectMode.multiple || this.features.includes(Feature.allowEmpty))) {
      this.logger.debug(`option ${index} has been removed`);
      this.selected.splice(index, 1);
      this.changes.selected++;
      this.onChange(this.mode === SelectMode.multiple ? this.selected : null);
    }
  }

  open() {
    this.opened = true;
    const input = this.queryRef.nativeElement;
    const focusQuery = () => {
      const style = getComputedStyle(input);
      if (style.display !== 'none') {
        setTimeout(() => input.focus());
      } else {
        setTimeout(() => focusQuery(), CHECKING_INTERVAL);
      }
    };
    focusQuery();
    if (!this.inline) {
      this.reference.popover = this.popover.show(this.hostRef, {
        contentTemplate: this.optionsTemplate,
        behaviour: Behaviour.dropdown,
        placement: this.placement,
        padding: UI.gutter.small
      });
    }
  }

  close() {
    this.opened = false;
    this.queryControl.setValue(null);
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
    this.cd.detectChanges();
  }

  writeValue(value: Key | Key[]) {
    if (this.mode === SelectMode.multiple && !Array.isArray(value)) {
      throw new Error('Wrong value form multiple select mode');
    }
    this.selected = (this.mode === SelectMode.single ? (!!value ? [value] : []) : value) as Key[];
    this.cd.detectChanges();
  }

  createOption(query, event: KeyboardEvent) {
    if (event.key === Keyboard.enter) {
      event.preventDefault();
    }
    if (!!query && event.key === Keyboard.enter && !!this.creator) {
      const complete = this.creator(query, this.close.bind(this));
      if (!!complete) {
        complete.subscribe(() => this.queryControl.setValue(null));
      }
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }
}
