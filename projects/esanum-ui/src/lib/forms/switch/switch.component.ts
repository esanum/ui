import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { EsanumUIConfig } from '../../config';
import { FlexAlign } from '../../core/enums/flex';
import { Size } from '../../core/enums/size';
import { SwitchStyle } from '../../core/enums/style';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';

type StateElements = {
  on?: string,
  off?: string
}

@Component({
  selector: 'sn-switch',
  templateUrl: './switch.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwitchComponent implements ControlValueAccessor, OnInit {

  @HostBinding('attr.sw')
  readonly host = '';

  ui = UI;
  styles = SwitchStyle;

  switchControl = this.fb.control(false);
  form = this.fb.group({
    switch: this.switchControl
  });

  private _label: string;
  private _icons: StateElements;
  private _tags: StateElements;
  private _style: SwitchStyle;
  private _value: any;
  private _align: FlexAlign = FlexAlign.center;
  private _size: Size = Size.normal;
  private _labelTemplate: TemplateRef<any>;

  @HostBinding('attr.data-sn-focused')
  focused = false;

  get checked() {
    return this.switchControl.value;
  }

  @Input()
  set label(label: string) {
    this._label = label;
    this.updated.emit();
  }

  get label(): string {
    return this._label;
  }

  @Input()
  set icons(icons: StateElements) {
    this._icons = icons;
    this.updated.emit();
  }

  get icons(): StateElements {
    return this._icons;
  }

  @Input()
  set tags(tags: StateElements) {
    this._tags = tags;
    this.updated.emit();
  }

  get tags(): StateElements {
    return this._tags;
  }

  @HostBinding('attr.data-sn-size')
  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.center;
  }

  get align() {
    return this._align;
  }

  @Input()
  set style(style: SwitchStyle) {
    this._style = style;
    this.cd.detectChanges();
    this.updated.emit();
  }

  get style() {
    return this._style || this.config.switch?.style || SwitchStyle.default;
  }

  @Input()
  set value(value: any) {
    this._value = value;
    this.updated.emit();
  }

  get value(): any {
    return this._value;
  }

  @ContentChild('switchLabelTemplate')
  set labelTemplate(template: TemplateRef<any>) {
    this._labelTemplate = template;
    this.cd.detectChanges();
    this.updated.emit();
  }

  get labelTemplate() {
    return this._labelTemplate;
  }

  get onIcon() {
    return this.icons?.on || this.config.switch?.icons?.on;
  }

  get offIcon() {
    return this.icons?.off || this.config.switch?.icons?.off;
  }

  @Output()
  updated = new EventEmitter<any>();

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private config: EsanumUIConfig,
              private logger: NGXLogger,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.switchControl.valueChanges
      .subscribe(value => this.onChange(value));
  }

  writeValue(value) {
    this.switchControl.setValue(value, {emitEvent: false});
    this.cd.detectChanges();
  }

  setDisabledState(disabled: boolean) {
    disabled ? this.switchControl.disable({emitEvent: false}) : this.switchControl.enable({emitEvent: false});
  }

}
