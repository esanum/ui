import { Directive, Input } from '@angular/core';

@Directive({
  selector: 'sn-week-metric'
})
export class WeekMetricDirective {

  @Input()
  title: string;

}
