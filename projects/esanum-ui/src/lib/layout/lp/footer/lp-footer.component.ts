import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'sn-lp-footer',
  templateUrl: './lp-footer.component.html'
})
export class LpFooterComponent {

  @HostBinding('attr.lf')
  readonly host = '';

}
