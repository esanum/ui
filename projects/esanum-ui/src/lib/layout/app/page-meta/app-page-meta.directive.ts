import { Directive, OnDestroy, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

type PageMeta = {
  title?: string,
  description?: string,
  image?: string
}

type MetaType = ((data: { [name: string]: any }, route: ActivatedRouteSnapshot) => PageMeta)
  | PageMeta
  | string;

@Directive({
  selector: 'sn-app-page-meta'
})
export class AppPageMetaDirective implements OnInit, OnDestroy {

  private destroy$ = new Subject<any>();

  constructor(private router: Router,
              private titleService: Title,
              private metaService: Meta) {
  }

  ngOnInit() {
    this.build();
    this.router.events.pipe(
      takeUntil(this.destroy$),
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => this.build());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private build() {
    const chunks: {
      meta: MetaType,
      route: ActivatedRouteSnapshot
    }[] = [];

    let current = this.router.routerState.snapshot.root;
    while (!!current) {
      const meta = current.data?.meta;
      if (!!meta) {
        chunks.push({meta, route: current});
      }
      current = current.firstChild;
    }

    for (let {meta, route} of chunks.reverse()) {
      const pageMeta = this.getMeta(meta, route);
      if (!!pageMeta) {
        this.setPageMeta(pageMeta);
        break;
      }
    }

  }

  private getMeta(meta: MetaType, snapshot: ActivatedRouteSnapshot): PageMeta {
    switch (typeof meta) {
      case 'string':
        return {title: meta};
      case 'object':
        return meta || {};
      case 'function':
        return meta(snapshot.data, snapshot);
      default:
        throw new Error(`wrong meta type: ${typeof meta}`);
    }
  }

  private setPageMeta(meta: PageMeta) {
    if (!!meta.title) {
      const decodeHTMLEntities = (input: string): string => {
        const textArea = document.createElement('textarea');
        textArea.innerHTML = input;
        return textArea.value;
      };

      const title = meta.title
        .replace(/<br(\/)*>/, ' ')
        .replace(/(<([^>]+)>)/ig, '');
      const decoded = decodeHTMLEntities(title);

      this.titleService.setTitle(decoded);
      this.setMetaProperty('og:title', decoded);
      this.setMetaName('twitter:title', decoded);
    }

    if (!!meta.description) {
      this.setMetaName('description', meta.description);
      this.setMetaProperty('og:description', meta.description);
      this.setMetaName('twitter:description', meta.description);
    }

    if (!!meta.image) {
      this.setMetaName('image', meta.image);
    }
  }

  private setMetaName(name: string, content: string) {
    if (!this.metaService.getTag(`name = "${name}"`)) {
      this.metaService.addTag({name, content: content});
    } else {
      this.metaService.updateTag({name, content: content});
    }
  }

  private setMetaProperty(property: string, content: string) {
    if (!this.metaService.getTag(`property = "${property}"`)) {
      this.metaService.addTag({property, content: content});
    } else {
      this.metaService.updateTag({property, content: content});
    }
  }
}
