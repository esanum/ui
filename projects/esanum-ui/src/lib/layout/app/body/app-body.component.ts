import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'sn-app-body',
  templateUrl: './app-body.component.html'
})
export class AppBodyComponent {

  @HostBinding('attr.ab')
  readonly host = '';
}
