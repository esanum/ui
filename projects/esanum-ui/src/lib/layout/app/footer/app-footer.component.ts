import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'sn-app-footer',
  templateUrl: './app-footer.component.html'
})
export class AppFooterComponent {

  @HostBinding('attr.af')
  readonly host = '';

}
