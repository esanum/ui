import { AfterViewInit, ChangeDetectionStrategy, Component, ContentChildren, HostBinding, Input, QueryList } from '@angular/core';
import { Feature } from '../../../core/enums/feature';
import { FlexAlign, FlexJustify } from '../../../core/enums/flex';
import { Gutter } from '../../../core/enums/gutter';
import { ColComponent, RowLayout } from '../col/col.component';

@Component({
  selector: 'sn-row',
  templateUrl: './row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RowComponent implements AfterViewInit {

  @HostBinding('attr.rw')
  readonly host = '';

  @HostBinding('attr.data-sn-align')
  _align: FlexAlign = FlexAlign.start;

  @HostBinding('attr.data-sn-gutter')
  _gutter: Gutter = Gutter.small;

  @HostBinding('attr.data-sn-spacing')
  @HostBinding('attr.data-sn-top-shift')
  _spacing: Gutter = Gutter.normal;

  @HostBinding('attr.data-sn-justify')
  _justify: FlexJustify = FlexJustify.start;

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.start;
  }

  @Input()
  set justify(justify: FlexJustify) {
    this._justify = justify || FlexJustify.start;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.normal;
  }

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.small;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChildren(ColComponent)
  columns: QueryList<ColComponent>;

  ngAfterViewInit() {
    this.columns.changes.subscribe(() => this.render());
    setTimeout(() => this.render(), 0);
  }

  private render() {
    if (this.features.includes(Feature.lines)) {
      let lines = this.getLines(column => column.forWide);
      this.setGrid(lines, (column, grid) => column.wideGrid = grid);

      lines = this.getLines(column => column.forDesktop);
      this.setGrid(lines, (column, grid) => column.desktopGrid = grid);

      lines = this.getLines(column => column.forTablet);
      this.setGrid(lines, (column, grid) => column.tabletGrid = grid);

      lines = this.getLines(column => column.forMobile);
      this.setGrid(lines, (column, grid) => column.mobileGrid = grid);
    }
  }

  private getLines(getter: (column: ColComponent) => number) {
    let counter = 0;
    let lines = [], line = [];
    const columns = this.columns.toArray();
    columns.forEach(column => {
      const span = column.span || getter(column);
      counter += span;
      if (counter <= 12) {
        line.push(column);
      } else {
        lines.push(line);
        line = [column];
        counter = span;
      }
    });
    lines.push(line);
    return lines;
  }

  private setGrid(lines: ColComponent[][], setter: (column: ColComponent, arrangement: RowLayout) => void) {
    for (let i = 0; i < lines.length; i++) {
      let line = lines[i];
      for (let j = 0; j < line.length; j++) {
        let column = line[j];
        setter(column, {
          firstInline: j == 0,
          lastInLine: j >= line.length - 1,
          firstLine: i == 0,
          lastLine: i >= lines.length - 1
        });
      }
    }
  }

}
