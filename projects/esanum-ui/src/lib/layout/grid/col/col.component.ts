import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

enum Overrides {
  tablet,
  desktop,
  wide
}

export type RowLayout = {
  firstInline: boolean,
  lastInLine: boolean,
  firstLine: boolean,
  lastLine: boolean
};

@Component({
  selector: 'sn-col',
  templateUrl: './col.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColComponent {

  @HostBinding('attr.cl')
  readonly host = '';

  private _mobile = 12;
  private _tablet = 6;
  private _desktop = 1;
  private _wide = 1;

  overrides: Overrides[] = [];

  @HostBinding('attr.data-sn-mobile')
  get forMobile() {
    return this._mobile;
  }

  @HostBinding('attr.data-sn-tablet')
  get forTablet() {
    return this.overrides.includes(Overrides.tablet) ? this._tablet : this.forMobile;
  }

  @HostBinding('attr.data-sn-desktop')
  get forDesktop() {
    return this.overrides.includes(Overrides.desktop) ? this._desktop : this.forTablet;
  }

  @HostBinding('attr.data-sn-wide')
  get forWide() {
    return this.overrides.includes(Overrides.wide) ? this._wide : this.forDesktop;
  }

  @Input()
  set mobile(mobile: number) {
    this._mobile = mobile;
  }

  @Input()
  set tablet(tablet: number) {
    this._tablet = tablet;
    this.overrides.push(Overrides.tablet);
  }

  @Input()
  set desktop(desktop: number) {
    this._desktop = desktop;
    this.overrides.push(Overrides.desktop);
  }

  @Input()
  set wide(wide: number) {
    this._wide = wide;
    this.overrides.push(Overrides.wide);
  }

  @HostBinding('attr.data-sn-span')
  @Input()
  span = null;

  // wide row layout

  @Input()
  wideGrid: RowLayout;

  @HostBinding('attr.data-sn-wide-first-in-line')
  get wideFirstInline() {
    return this.wideGrid?.firstInline;
  };

  @HostBinding('attr.data-sn-wide-last-in-line')
  get wideLastInLine() {
    return this.wideGrid?.lastInLine;
  };

  @HostBinding('attr.data-sn-wide-first-line')
  get wideFirstLine() {
    return this.wideGrid?.firstLine;
  };

  @HostBinding('attr.data-sn-wide-last-line')
  get wideLastLine() {
    return this.wideGrid?.lastLine;
  };

  // desktop row layout

  @Input()
  desktopGrid: RowLayout;

  @HostBinding('attr.data-sn-desktop-first-in-line')
  get desktopFirstInline() {
    return this.desktopGrid?.firstInline;
  };

  @HostBinding('attr.data-sn-desktop-last-in-line')
  get desktopLastInLine() {
    return this.desktopGrid?.lastInLine;
  };

  @HostBinding('attr.data-sn-desktop-first-line')
  get desktopFirstLine() {
    return this.desktopGrid?.firstLine;
  };

  @HostBinding('attr.data-sn-desktop-last-line')
  get desktopLastLine() {
    return this.desktopGrid?.lastLine;
  };

  // tablet row layout

  @Input()
  tabletGrid: RowLayout;

  @HostBinding('attr.data-sn-tablet-first-in-line')
  get tabletFirstInline() {
    return this.tabletGrid?.firstInline;
  };

  @HostBinding('attr.data-sn-tablet-last-in-line')
  get tabletLastInLine() {
    return this.tabletGrid?.lastInLine;
  };

  @HostBinding('attr.data-sn-tablet-first-line')
  get tabletFirstLine() {
    return this.tabletGrid?.firstLine;
  };

  @HostBinding('attr.data-sn-tablet-last-line')
  get tabletLastLine() {
    return this.tabletGrid?.lastLine;
  };

  // mobile row layout

  @Input()
  mobileGrid: RowLayout;

  @HostBinding('attr.data-sn-mobile-first-in-line')
  get mobileFirstInline() {
    return this.mobileGrid?.firstInline;
  };

  @HostBinding('attr.data-sn-mobile-last-in-line')
  get mobileLastInLine() {
    return this.mobileGrid?.lastInLine;
  };

  @HostBinding('attr.data-sn-mobile-first-line')
  get mobileFirstLine() {
    return this.mobileGrid?.firstLine;
  };

  @HostBinding('attr.data-sn-mobile-last-line')
  get mobileLastLine() {
    return this.mobileGrid?.lastLine;
  };

}
