import { Directive, Input } from '@angular/core';
import { Color } from '../../../core/enums/color';
import { UI } from '../../../core/enums/ui';

@Directive({
  selector: 'sn-progress-line'
})
export class ProgressLineDirective {

  ui = UI;

  @Input()
  color: string = Color.purpleLight;

  @Input()
  from: number;
}
