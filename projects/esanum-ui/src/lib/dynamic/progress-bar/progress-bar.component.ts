import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  HostBinding,
  Input,
  QueryList,
  TemplateRef
} from '@angular/core';
import { Color } from '../../core/enums/color';
import { ProgressBarStyle } from '../../core/enums/style';
import { UI } from '../../core/enums/ui';
import { ProgressLineDirective } from './line/progress-line.directive';

@Component({
  selector: 'sn-progress-bar',
  templateUrl: './progress-bar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent implements AfterViewInit {

  ui = UI;

  private _value = 0;
  private _style: ProgressBarStyle = ProgressBarStyle.default;

  @HostBinding('attr.pb')
  readonly host = '';

  @ContentChild('progressBarLegendTemplate')
  progressBarLegendTemplate: TemplateRef<any>;

  @Input()
  set value(value: number) {
    this._value = value || 0;
  }

  get value() {
    return this._value;
  }

  @Input()
  color: string = Color.purple;

  @Input()
  @HostBinding('attr.data-sn-style')
  set style(style: ProgressBarStyle) {
    this._style = style || ProgressBarStyle.default;
  }

  get style() {
    return this._style;
  }

  @ContentChildren(ProgressLineDirective)
  lines: QueryList<ProgressLineDirective>;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.lines.changes.subscribe(() => this.cd.detectChanges());
  }

}
