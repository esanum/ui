import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SliderModule } from '../../forms/slider/slider.module';
import { FormModule } from '../../forms/form/form.module';
import { SwitcherModule } from '../../forms/switcher/switcher.module';
import { SpinnerModule } from '../../layout/spinner/spinner.module';
import { StackModule } from '../../layout/stack/stack.module';
import { IconModule } from '../../elements/icon/icon.module';
import { TextPipesModule } from '../../core/pipes/text-pipes.module';
import { ButtonModule } from '../../forms/button/button.module';
import { InPaintComponent } from './in-paint.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    TextPipesModule,
    IconModule,
    StackModule,
    SpinnerModule,
    FormModule,
    ReactiveFormsModule,
    SwitcherModule,
    SliderModule
  ],
  exports: [
    InPaintComponent
  ],
  declarations: [
    InPaintComponent
  ],
  entryComponents: [
    InPaintComponent
  ]
})
export class InPaintModule {
}
