import { Component, ElementRef, HostBinding, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { UI } from '../../core/enums/ui';
import { ModalService } from '../../overlays/modal/modal.service';

const DEFAULT_BRASH_SIZE = 10;
const MIN_BRASH_SIZE = 5;
const MAX_BRASH_SIZE = 30;
const STEP_BRASH_SIZE = 1;
const DEFAULT_CANVAS_WIDTH = 500;
const DEFAULT_CANVAS_HEIGHT = 500;

enum Mode {
  brash = 'brash',
  eraser = 'eraser'
}

enum View {
  chooseFile = 'choose_file',
  draw = 'draw'
}

@Component({
  selector: 'sn-in-paint',
  templateUrl: './in-paint.component.html'
})
export class InPaintComponent implements OnInit {

  ui = UI;

  @HostBinding('attr.ip')
  readonly host = '';

  @ViewChild('pictureCanvasRef')
  pictureCanvasRef: ElementRef<HTMLCanvasElement>;

  @ViewChild('paintingCanvasRef')
  paintingCanvasRef: ElementRef<HTMLCanvasElement>;

  @ViewChild('modalTemplate')
  modalTemplate: TemplateRef<any>;

  mode = Mode;
  paintView = View;
  loading = false;
  min = MIN_BRASH_SIZE;
  max = MAX_BRASH_SIZE;
  step = STEP_BRASH_SIZE;

  ctxPicture: CanvasRenderingContext2D;
  ctxPainting: CanvasRenderingContext2D;
  paintingCanvas: HTMLCanvasElement;
  pictureCanvas: HTMLCanvasElement;
  src: SafeUrl;
  history = [];
  view: View = View.chooseFile;
  onMouseMoveBound: () => any;

  modeControl = this.fb.control(Mode.brash);
  sizeControl = this.fb.control(DEFAULT_BRASH_SIZE);
  form = this.fb.group({
    mode: this.modeControl,
    size: this.sizeControl
  });

  constructor(private modal: ModalService,
              private sanitizer: DomSanitizer,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.sizeControl.valueChanges
      .subscribe(value => this.renderCursor(value / 2));
  }

  setListeners() {
    if (!this.onMouseMoveBound) {
      this.onMouseMoveBound = this.draw.bind(this);
    }

    const removeListeners = () => {
      this.ctxPainting.beginPath();
      this.paintingCanvas.removeEventListener('mousemove', this.onMouseMoveBound);
    };

    this.paintingCanvas.addEventListener('mousedown', () =>
      this.paintingCanvas.addEventListener('mousemove', this.onMouseMoveBound));

    this.paintingCanvas.addEventListener('mouseup', () => {
      this.saveInHistory();
      removeListeners();
    });

    this.paintingCanvas.addEventListener('mouseover', () => removeListeners());
  }

  load({target}: { target: HTMLInputElement }) {
    this.loading = true;
    const file = target.files[0];
    const fr = new FileReader();
    fr.readAsDataURL(file);
    fr.onload = () => {
      const img = new Image();
      img.crossOrigin = 'anonymous';
      img.src = fr.result.toString();
      this.view = View.draw;
      this.loading = false;
      img.onload = () => {
        this.pictureCanvas = this.pictureCanvasRef.nativeElement;
        this.paintingCanvas = this.paintingCanvasRef?.nativeElement;

        const hRatio = DEFAULT_CANVAS_WIDTH / img.width;
        const vRatio = DEFAULT_CANVAS_HEIGHT / img.height;
        const ratio = Math.min(hRatio, vRatio);

        this.pictureCanvas.width = this.paintingCanvas.width = img.width * ratio;
        this.pictureCanvas.height = this.paintingCanvas.height = img.height * ratio;

        this.ctxPicture = this.pictureCanvas.getContext('2d');
        this.ctxPicture.clearRect(0, 0, this.pictureCanvas.width, this.pictureCanvas.height);
        this.ctxPicture.drawImage(img, 0, 0, img.width, img.height, 0, 0, this.pictureCanvas.width, this.pictureCanvas.height);

        this.ctxPainting = this.paintingCanvas.getContext('2d');
        this.ctxPainting.beginPath();

        this.saveInHistory();
        this.setListeners();
        this.renderCursor(this.sizeControl.value / 2);
      };
    };
  }

  saveInHistory() {
    this.history.unshift(this.ctxPainting.getImageData(0, 0, this.paintingCanvas.width, this.paintingCanvas.height));
  }

  save() {
    this.ctxPicture.drawImage(this.ctxPainting.canvas, 0, 0);
    this.history = [];
    this.saveInHistory();
    this.pictureCanvas.toBlob(blob => {
      this.src = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob));
      this.modal.open(this.modalTemplate, {title: {text: 'image'}});
    });
  }

  clear() {
    this.ctxPainting.clearRect(0, 0, this.paintingCanvas.width, this.paintingCanvas.height);
  }

  undo() {
    if (this.history.length > 1) {
      this.ctxPainting.clearRect(0, 0, this.paintingCanvas.width, this.paintingCanvas.height);
      this.ctxPainting.putImageData(this.history[1], 0, 0);
      this.history.shift();
    }
  }

  draw(event) {
    const x = event.layerX;
    const y = event.layerY;

    switch (this.modeControl.value) {
      case Mode.brash:
        this.ctxPainting.lineWidth = this.sizeControl.value;
        this.ctxPainting.lineCap = 'round';
        this.ctxPainting.lineJoin = 'round';
        this.ctxPainting.lineTo(x, y);
        this.ctxPainting.stroke();
        break;
      case Mode.eraser:
        this.ctxPainting.clearRect(x, y, this.sizeControl.value, this.sizeControl.value);
        break;
    }
  }

  renderCursor(size) {
    const padding = 2;
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = size * 2 + padding;
    canvas.height = size * 2 + padding;
    ctx.arc(canvas.width / 2, canvas.height / 2, size, 0, Math.PI * 2, true);
    ctx.fill();
    const url = canvas.toDataURL();
    this.paintingCanvas.style.cursor = `url(${url}) ${size + padding} ${size + padding}, auto`;
  }
}
