import { animate, state, style, transition, trigger } from '@angular/animations';

export const CAROUSEL_ANIMATION = [
  trigger('visibility', [
    state('slide-hidden-left', style({transform: 'translateX(100%)'})),
    state('slide-hidden-right', style({transform: 'translateX(-100%)'})),

    transition('void => slide-visible-left', [
      style({transform: 'translateX(0%)'})
    ]),
    transition('void => slide-visible-right', [
      style({transform: 'translateX(0%)'})
    ]),
    transition('void => slide-hidden-left', [
      style({transform: 'translateX(100%)'})
    ]),
    transition('void => slide-hidden-right', [
      style({transform: 'translateX(-100%)'})
    ]),

    transition('slide-hidden-right => slide-visible-right', [
      style({transform: 'translateX(100%)'}),
      animate('{{speed}}ms ease', style({transform: 'translateX(0%)'}))
    ], {params: {speed: '300'}}),
    transition('slide-hidden-right => slide-visible-left', [
      style({transform: 'translateX(-100%)'}),
      animate('{{speed}}ms ease', style({transform: 'translateX(0%)'}))
    ], {params: {speed: '300'}}),

    transition('slide-hidden-left => slide-visible-left', [
      style({transform: 'translateX(-100%)'}),
      animate('{{speed}}ms ease', style({transform: 'translateX(0%)'}))
    ], {params: {speed: '300'}}),
    transition('slide-hidden-left => slide-visible-right', [
      style({transform: 'translateX(100%)'}),
      animate('{{speed}}ms ease', style({transform: 'translateX(0%)'}))
    ], {params: {speed: '300'}}),

    transition('slide-visible-right => slide-hidden-right', [
      animate('{{speed}}ms ease', style({transform: 'translateX(-100%)'}))
    ], {params: {speed: '300'}}),
    transition('slide-visible-right => slide-hidden-left', [
      animate('{{speed}}ms ease', style({transform: 'translateX(100%)'}))
    ], {params: {speed: '300'}}),

    transition('slide-visible-left => slide-hidden-right', [
      animate('{{speed}}ms ease', style({transform: 'translateX(-100%)'}))
    ], {params: {speed: '300'}}),
    transition('slide-visible-left => slide-hidden-left', [
      animate('{{speed}}ms ease', style({transform: 'translateX(100%)'}))
    ], {params: {speed: '300'}}),

    state('fade-hidden-left', style({opacity: 0})),
    state('fade-hidden-right', style({opacity: 0})),

    transition('void => fade-visible-left', [
      style({opacity: 1})
    ]),
    transition('void => fade-visible-right', [
      style({opacity: 1})
    ]),
    transition('void => fade-hidden-left', [
      style({opacity: 0})
    ]),
    transition('void => fade-hidden-right', [
      style({opacity: 0})
    ]),

    transition('fade-hidden-right => fade-visible-right', [
      style({opacity: 0}),
      animate('{{speed}}ms ease-in', style({opacity: 1}))
    ], {params: {speed: '500'}}),
    transition('fade-hidden-right => fade-visible-left', [
      style({opacity: 0}),
      animate('{{speed}}ms ease-in', style({opacity: 1}))
    ], {params: {speed: '500'}}),

    transition('fade-hidden-left => fade-visible-left', [
      style({opacity: 0}),
      animate('{{speed}}ms ease-in', style({opacity: 1}))
    ], {params: {speed: '500'}}),
    transition('fade-hidden-left => fade-visible-right', [
      style({opacity: 0}),
      animate('{{speed}}ms ease-in', style({opacity: 1}))
    ], {params: {speed: '500'}}),

    transition('fade-visible-right => fade-hidden-right', [
      animate('{{speed}}ms ease-out', style({opacity: 0}))
    ], {params: {speed: '500'}}),
    transition('fade-visible-right => fade-hidden-left', [
      animate('{{speed}}ms ease-out', style({opacity: 0}))
    ], {params: {speed: '500'}}),

    transition('fade-visible-left => fade-hidden-right', [
      animate('{{speed}}ms ease-out', style({opacity: 0}))
    ], {params: {speed: '500'}}),
    transition('fade-visible-left => fade-hidden-left', [
      animate('{{speed}}ms ease-out', style({opacity: 0}))
    ], {params: {speed: '500'}})
  ])
];
