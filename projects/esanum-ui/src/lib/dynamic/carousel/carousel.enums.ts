export enum CarouselOrientation {
  left = 'left',
  right = 'right'
}

export enum CarouselVisibility {
  visible = 'visible',
  hidden = 'hidden'
}

export enum CarouselAnimation {
  slide = 'slide',
  fade = 'fade'
}
