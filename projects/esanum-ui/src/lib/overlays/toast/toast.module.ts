import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TextPipesModule } from '../../core/pipes/text-pipes.module';
import { IconModule } from '../../elements/icon/icon.module';
import { ButtonModule } from '../../forms/button/button.module';
import { StackModule } from '../../layout/stack/stack.module';
import { ToastItemComponent } from './toast-item/toast-item.component';
import { ToastComponent } from './toast.component';
import { ToastDirective } from './toast.directive';

@NgModule({
  declarations: [
    ToastComponent,
    ToastDirective,
    ToastItemComponent
  ],
  imports: [
    CommonModule,
    StackModule,
    TextPipesModule,
    ButtonModule,
    IconModule
  ],
  entryComponents: [
    ToastComponent,
    ToastItemComponent
  ],
  exports: [
    ToastComponent,
    ToastDirective,
    ToastItemComponent
  ]
})
export class ToastModule {
}
