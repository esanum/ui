import { Directive, HostListener, Input } from '@angular/core';
import { ToastService } from './toast.service';
import { ToastOptions } from './toast.type';

@Directive({
  selector: '[snToast]',
  exportAs: 'snToast'
})
export class ToastDirective {

  @Input('snToast')
  options: Partial<ToastOptions>;

  constructor(private toastService: ToastService) {
  }

  @HostListener('click')
  click() {
    this.toastService.show(this.options);
  }
}
