import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { UI } from '../../core/enums/ui';
import { ToastItemComponent } from './toast-item/toast-item.component';
import { ToastService } from './toast.service';
import { ToastOptions } from './toast.type';

@Component({
  selector: 'sn-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastComponent implements OnInit, OnDestroy {

  ui = UI;

  @HostBinding('attr.tt')
  readonly host = '';

  @ViewChild('toastItems', {read: ViewContainerRef})
  toastItems: ViewContainerRef;

  constructor(private toastService: ToastService,
              private hostRef: ElementRef,
              private renderer: Renderer2,
              private cd: ChangeDetectorRef,
              private cfr: ComponentFactoryResolver) {
  }

  ngOnInit() {
    this.toastService.register(this);
  }

  ngOnDestroy() {
    this.toastService.unregister();
  }

  show() {
    if (this.toastItems.length <= 1) {
      this.renderer.setStyle(this.hostRef.nativeElement, 'display', 'block');
    }
  }

  hide() {
    if (this.toastItems.length === 0) {
      this.renderer.setStyle(this.hostRef.nativeElement, 'display', 'none');
    }
  }

  addItem(options: Partial<ToastOptions>) {
    const toastItemFactory = this.cfr.resolveComponentFactory(ToastItemComponent);
    const component = this.toastItems.createComponent(toastItemFactory);
    component.instance.options = options;
    component.instance.closed
      .subscribe(() => {
        component.destroy();
        this.hide();
      });

    this.show();
    this.cd.detectChanges();
  }
}
