import { ComponentRef, TemplateRef } from '@angular/core';
import { Scheme } from '../../core/enums/scheme';
import { Size } from '../../core/enums/size';

export class ToastOptions {
  title?: string;
  message?: string;
  content?: TemplateRef<any> | ComponentRef<any>;
  icon?: string;
  scheme?: Scheme;
  size?: Size;
  closeDelay?: number;

  constructor(defs: Partial<ToastOptions> = null) {
    Object.assign(this, defs);
  }
}
