import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  EventEmitter,
  HostBinding,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Scheme } from '../../../core/enums/scheme';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { ToastOptions } from '../toast.type';
import { ANIMATION_DURATION, AnimationState, VISIBILITY } from './toast-item.animation';

const CLOSE_DELAY = 7000;
const DEFAULT_ICONS = {
  'primary': UI.icons.information,
  'secondary': UI.icons.information,
  'success': UI.icons.checked,
  'fail': UI.icons.stop,
  'accent': UI.icons.warning
};

@Component({
  selector: 'sn-toast-item',
  templateUrl: './toast-item.component.html',
  styleUrls: ['./toast-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [VISIBILITY]
})
export class ToastItemComponent implements AfterViewInit {

  ui = UI;

  private closing = false;
  private _options: ToastOptions;

  icon: string;
  state = AnimationState.hide;
  closed = new EventEmitter<ToastItemComponent>();
  contentTemplate: TemplateRef<any>;

  @HostBinding('attr.ti')
  readonly host = '';

  @HostBinding('attr.data-sn-scheme')
  scheme: Scheme;

  @HostBinding('attr.data-sn-size')
  size: Size;

  set options(options: Partial<ToastOptions>) {
    this._options = options;
    this.render();
  }

  get options() {
    return this._options;
  }

  @ViewChild('container', {read: ViewContainerRef, static: false})
  container: ViewContainerRef

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.contentTemplate = null;
    this.container.clear();
    if (this.options.content instanceof TemplateRef) {
      this.contentTemplate = this.options.content;
    } else if (this.options.content instanceof ComponentRef) {
      this.container.insert(this.options.content.hostView);
    }

    this.state = AnimationState.show;
    this.cd.detectChanges();

    setTimeout(() => {
      if (!this.closing) {
        this.close();
      }
    }, this.options.closeDelay || CLOSE_DELAY);
  }

  private render() {
    this.scheme = this.options.scheme || Scheme.primary;
    this.icon = this.options.icon || DEFAULT_ICONS[this.scheme];
    this.size = this.options.size || Size.small;
  }

  close() {
    this.state = AnimationState.hide;
    this.cd.detectChanges();

    setTimeout(() => {
      this.closing = true;
      this.closed.emit();
    }, ANIMATION_DURATION);
  }
}
