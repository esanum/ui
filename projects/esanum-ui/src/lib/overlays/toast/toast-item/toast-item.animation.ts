import { animate, state, style, transition, trigger } from '@angular/animations';

export const ANIMATION_DURATION = 500;

export enum AnimationState {
  show = 'show',
  hide = 'hide'
}

const SHOW_TOAST_STYLE = {
  transform: 'translateX(0)',
  opacity: 1
};

const HIDE_TOAST_STYLE = {
  transform: 'translateX(400px)',
  opacity: 0
};

export const VISIBILITY =
  trigger('visibility', [
    state(`${AnimationState.show}`, style(SHOW_TOAST_STYLE)),
    state(`${AnimationState.hide}`, style(HIDE_TOAST_STYLE)),

    transition(`${AnimationState.show} <=> ${AnimationState.hide}`,
      animate(`${ANIMATION_DURATION}ms ease-in-out`))
  ]);
