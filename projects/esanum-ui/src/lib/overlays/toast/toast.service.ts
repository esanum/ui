import { Injectable } from '@angular/core';
import { ToastComponent } from './toast.component';
import { ToastOptions } from './toast.type';

@Injectable({providedIn: 'root'})
export class ToastService {

  private toast: ToastComponent;

  private checkRegistration() {
    if (!this.toast) {
      throw new Error('toast component is not registered');
    }
  }

  register(toast: ToastComponent) {
    this.toast = toast;
  }

  unregister() {
    this.toast = null;
  }

  show(options: Partial<ToastOptions>) {
    this.checkRegistration();
    this.toast.addItem(options);
  }
}
