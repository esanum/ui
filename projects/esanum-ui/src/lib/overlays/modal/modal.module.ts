import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TextPipesModule } from '../../core/pipes/text-pipes.module';
import { IconModule } from '../../elements/icon/icon.module';
import { ButtonModule } from '../../forms/button/button.module';
import { BlockModule } from '../../layout/block/block.module';
import { ResponsiveModule } from '../../layout/responsive/responsive.module';
import { StackModule } from '../../layout/stack/stack.module';
import { ModalComponent } from './modal.component';
import { ModalDirective } from './modal.directive';

@NgModule({
  declarations: [
    ModalComponent,
    ModalDirective
  ],
  imports: [
    CommonModule,
    BlockModule,
    ButtonModule,
    IconModule,
    ResponsiveModule,
    StackModule,
    TextPipesModule
  ],
  entryComponents: [
    ModalComponent
  ],
  exports: [
    ModalComponent,
    ModalDirective
  ]
})
export class ModalModule {
}
