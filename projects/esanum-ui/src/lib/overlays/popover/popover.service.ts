import {ElementRef, EventEmitter, Injectable, NgZone, Renderer2, RendererFactory2} from '@angular/core';
import {PopoverComponent, PopoverOptions} from './popover.component';

// TODO: move to interface and check ngc warnings
export class PopoverInstance {
  hide: () => void;
  picked: (path: Object[]) => boolean;
  update: () => void;
}

@Injectable({providedIn: 'root'})
export class PopoverService {

  private popover: PopoverComponent;
  private renderer: Renderer2;
  private target: ElementRef;
  private listeners: Function[] = [];
  attached = new EventEmitter<ElementRef>();

  constructor(private zone: NgZone,
              public rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  register(popover: PopoverComponent): void {
    this.popover = popover;
  }

  unregister(): void {
    this.popover = null;
  }

  private checkRegistration() {
    if (!this.popover) {
      throw new Error('popover component is not registered');
    }
  }

  private listenDocumentScroll() {
    this.zone.runOutsideAngular(() => {
      let mouseX = 0, mouseY = 0;
      this.listeners.push(this.renderer.listen('document', 'mousemove', ({clientX, clientY}: MouseEvent) => {
        [mouseX, mouseY] = [clientX, clientY];
      }));
      this.listeners.push(this.renderer.listen('document', 'scroll', () => {
        if (!this.popover.contains(document.elementFromPoint(mouseX, mouseY))) {
          this.popover.hide();
          this.unlistenDocumentScroll();
          // TODO: waiting for OnPush
          // https://github.com/angular/angular/issues/11442
          // https://github.com/angular/angular/issues/22560
          this.zone.run(() => this.attached.emit(null));
        }
      }));
    });
  }

  private unlistenDocumentScroll() {
    this.listeners.forEach(listener => listener());
  }

  show(target: ElementRef, options: Partial<PopoverOptions>): PopoverInstance {
    this.checkRegistration();

    this.target = target;
    this.popover.show(target, options);

    this.listenDocumentScroll();
    this.attached.emit(target);

    return {
      hide: () => this.hide(target),
      picked: (path: Object[]) => this.popover.picked(path),
      update: () => this.popover.update()
    };
  }

  private hide(target: ElementRef = null) {
    if (!this.target || this.target === target) {
      this.popover.hide();
      this.unlistenDocumentScroll();
      this.attached.emit(null);
    }
  }
}
