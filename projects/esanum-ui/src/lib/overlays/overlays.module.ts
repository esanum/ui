import { NgModule } from '@angular/core';
import { ModalModule } from './modal/modal.module';
import { PopoverModule } from './popover/popover.module';
import { ToastModule } from './toast/toast.module';

@NgModule({
  exports: [
    ModalModule,
    PopoverModule,
    ToastModule
  ]
})
export class OverlaysModule {
}
