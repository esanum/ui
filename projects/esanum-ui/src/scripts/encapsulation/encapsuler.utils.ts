import { existsSync, writeFileSync } from 'fs';
import * as path from 'path';

const nodeModulesFiles = '!node_modules/**';
const buildFiles = './**/build.json';

export class EncapsulerUtils {

  static getBuilder(filePath: string): string[] {
    let builder = [buildFiles, nodeModulesFiles];

    if (!!filePath && typeof filePath === 'string') {
      const builderPath = EncapsulerUtils.findBuilder(filePath);
      if (!!builderPath) {
        builder = [builderPath];
      }
    }

    return builder;
  }

  static findBuilder(file: string): string {
    let builderPath = `${path.dirname(file)}/build.json`;

    while (builderPath) {
      if (existsSync(builderPath)) {
        return builderPath;
      }
      if (!existsSync(builderPath)) {
        const dir = path.dirname(builderPath);
        const prevFolder = dir.substring(0, dir.lastIndexOf('/'));

        if (!!prevFolder) {
          builderPath = `${prevFolder}/build.json`;
        } else {
          return null;
        }
      }
    }
    return null;
  }

  static saveFile(filePath: string, oldContent: string, newContent: string) {
    if (!!newContent && oldContent !== newContent) {
      writeFileSync(filePath, newContent);
    }
  }

  static clearImport(importFile: string): string {
    return importFile
      .replace(/";|';/, '')
      .replace(/@import ["|']/, '@import \'../')
      .split('/')
      .slice(0, 3)
      .join('/') + '\';';
  }

  static clearContent(content: string): string {
    return content
      .replace(/@import.*$/gm, '')
      .replace(/(\n){2,}/gm, '\n');
  }

}
