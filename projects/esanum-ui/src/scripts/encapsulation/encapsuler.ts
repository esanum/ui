import { appendFileSync, existsSync, mkdirSync, readdirSync, readFileSync, writeFileSync } from 'fs';
import * as gulp from 'gulp';
import * as debug from 'gulp-debug';
import { Gulpclass, SequenceTask, Task } from 'gulpclass';
import * as map from 'map-stream';
import * as path from 'path';
import 'reflect-metadata';
import { Encapsuler, EncapsulerComponent } from './encapsuler.model';
import { EncapsulerUtils } from './encapsuler.utils';

const stylesFiles = './src/lib/assets/styles';
const mixinsFile = './src/lib/assets/styles/sn-mixins.scss';
const hostsFile = './src/lib/assets/styles/sn-hosts.scss';
const buildFiles = './**/build.json';
const watchFiles = './src/lib/**/*.scss';
const ignoreWatchFiles = '!./src/lib/assets/styles/**/*.scss';
const argument = require('minimist')(process.argv.slice(2));

@Gulpclass()
export class Gulpfile {

  encapsuler: Encapsuler;
  dirname: string;
  hosts = {};
  names = {};

  @SequenceTask()
  build() {
    return ['encapsulate', 'mixins', 'checkHosts', 'setHosts'];
  }

  @Task()
  mixins() {
    return gulp.src([mixinsFile])
      .pipe(map((file, cb) => {
        const imports = readdirSync(stylesFiles, {withFileTypes: true})
          .filter(dirent => dirent.isDirectory())
          .map(dirent => `@import './${dirent.name}.scss';`)
          .join('\r\n');
        if (file.contents.toString() === imports) {
          return cb();
        }
        file.contents = Buffer.from(imports);
        return cb(null, file);
      }))
      .pipe(gulp.dest(stylesFiles));
  }

  @Task()
  checkHosts() {
    this.hosts = {};
    this.names = {};
    return gulp.src([buildFiles])
      .pipe(map((file, cb) => {
        const encapsuler = new Encapsuler(JSON.parse(file.contents.toString()));
        encapsuler.components
          .filter(component => !!component.host)
          .forEach(component => {
            this.hosts[component.host] = ++this.hosts[component.host] || 1;
            this.names[component.host] = component.name;
          });

        return cb();
      }));
  }

  @Task()
  setHosts() {
    const duplicates = [];
    for (let host in this.hosts) {
      if (this.hosts[host] > 1) {
        duplicates.push(host);
      }
    }
    if (duplicates.length > 0) {
      throw Error(`Duplicated hosts: ${duplicates.join(', ')}`);
    }

    return gulp.src([hostsFile])
      .pipe(map((file, cb) => {
        const variables = Object.keys(this.names)
          .sort((host1, host2) => host1.localeCompare(host2))
          .map(host => `$sn-${this.names[host]}-host: '[${host}]';`)
          .join('\n');
        file.contents = Buffer.from(variables);
        return cb(null, file);
      }))
      .pipe(gulp.dest(stylesFiles));
  }

  @SequenceTask()
  encapsulate(file = null) {
    return gulp.src(EncapsulerUtils.getBuilder(file || argument.file))
      .pipe(debug())
      .pipe(map((file, cb) => {
        this.encapsuler = new Encapsuler(JSON.parse(file.contents.toString()));
        this.dirname = path.dirname(file.path);

        if (!this.encapsuler.components) {
          return cb();
        }

        this.encapsulateScss();

        this.encapsuler.components
          .filter(component => !!component.html)
          .forEach(component => this.encapsulateHtml(component));

        return cb();
      }));
  }

  @SequenceTask()
  watch() {
    return gulp.watch([watchFiles, ignoreWatchFiles]).on('change', (e) => this.encapsulate(e));
  }

  private encapsulateHtml(component: EncapsulerComponent) {
    const htmlPath = path.normalize(`${this.dirname}/${component.html}`);
    if (existsSync(htmlPath)) {
      const oldContent = readFileSync(htmlPath).toString();
      const newContent = component.encapsulateHtml(oldContent);
      EncapsulerUtils.saveFile(htmlPath, oldContent, newContent);
    }
  }

  private encapsulateScss() {
    this.createSection();
    this.addToSection();

    const scssPath = `${stylesFiles}/${this.encapsuler.section}/${this.encapsuler.name}.scss`;
    const oldContent = existsSync(scssPath)
      ? readFileSync(scssPath).toString() : '';
    const newContent = this.combineStyles();
    EncapsulerUtils.saveFile(scssPath, oldContent, newContent);
  }

  private createSection() {
    const dirname = `${stylesFiles}/${this.encapsuler.section}`;
    if (!existsSync(dirname)) {
      mkdirSync(dirname, {recursive: true});
      writeFileSync(`${dirname}.scss`, '');
    }
  }

  private addToSection() {
    const sectionPath = `${stylesFiles}/${this.encapsuler.section}.scss`;
    const sectionContent = readFileSync(sectionPath).toString();
    const fileImport = `@import './${this.encapsuler.section}/${this.encapsuler.name}';\n`;

    if (!sectionContent.includes(fileImport)) {
      appendFileSync(sectionPath, fileImport);
    }
  }

  private combineStyles() {
    let content = '';

    if (this.encapsuler.additional.length > 0) {
      content = this.encapsuler.additional
        .map(style => readFileSync(path.normalize(`${this.dirname}/${style}`)).toString())
        .join('\n\r') + content;
    }

    content += this.encapsuler.components
      .filter(component => !!component.scss)
      .map(component => {
        const filePath = path.normalize(`${this.dirname}/${component.scss}`);
        const content = readFileSync(filePath).toString();
        return component.encapsulateScss(content);
      })
      .join('\n\r');

    return this.clearImports(content);
  }

  private clearImports(content: string) {
    const dir = `${this.encapsuler.section}/${this.encapsuler.name}`;
    let imports = (content.match(/@import.*$/gm) || [])
      .filter(file => !file.includes('sn-variables') && !file.includes(dir));

    imports = [...(new Set(imports.map(file => EncapsulerUtils.clearImport(file))))];
    const cleared = `${imports.join('\n')}\n${EncapsulerUtils.clearContent(content)}`;
    return `@import '../sn-variables';\n${cleared}`;
  }

}

