import { HTMLElement, Node, parse } from 'node-html-parser';
import * as createQueryWrapper from 'query-ast';
import { parse as scssParce, stringify } from 'scss-parser';

const IDENTIFIER_TYPE = 'identifier';
const SELECTOR_TYPE = 'selector';
const PSEUDO_CLASS_TYPE = 'pseudo_class';
const ATTRIBUTE_TYPE = 'attribute';
const ARGUMENTS_TYPE = 'arguments';

export class Encapsuler {

  host: string;
  section: string;
  name: string;
  additional: string[] = [];
  components: EncapsulerComponent[] = [];

  constructor(defs: Partial<Encapsuler>) {
    Object.assign(this, defs);
    this.components = this.components
      .map(component => new EncapsulerComponent(component));
  }

}

export class EncapsulerComponent {
  host: string;
  name: string;
  html: string;
  scss: string;

  constructor(defs: Partial<Encapsuler>) {
    Object.assign(this, defs);
  }

  encapsulateHtml(content: string): string {
    if (!this.html || !content) {
      return null;
    }

    const html = parse(content);
    html.childNodes = this.setHosts(html);
    return html.toString();
  }

  private setHosts(node: HTMLElement): Node[] {
    return node.childNodes.map(node => {
      if (node instanceof HTMLElement) {
        node.rawAttrs = this.updateRawAttrs(node);
        node.childNodes = this.setHosts(node);
      }
      return node;
    });
  }

  private updateRawAttrs(node: HTMLElement): string {
    let rawAttrs = node.rawAttrs;
    const regex = new RegExp(`_${this.host}\\b`);
    if (!node.tagName.startsWith('NG-') && !regex.test(rawAttrs)) {
      rawAttrs = `_${this.host} ${node.rawAttrs}`.trim();
    }
    return rawAttrs;
  }

  encapsulateScss(content: string): string {
    if (!this.scss || !content) {
      return '';
    }

    const host = `[${this.host}]`;
    const child = `[_${this.host}]`;

    if (!!this.host) {
      const $ = createQueryWrapper(scssParce(content));

      const query = $(n => n.node.type === IDENTIFIER_TYPE && n.parent.node.type === SELECTOR_TYPE
        || (n.node.type === ATTRIBUTE_TYPE && n.parent.node.value[0].value !== '&'));

      query.nodes.forEach((n, index) => {
        if (!n.node.value.includes('webkit') && n.parent.node.type !== ARGUMENTS_TYPE) {
          query.eq(index).after({value: child});
        }
      });

      const hostQuery = $(n => n.node.type === IDENTIFIER_TYPE
        && n.parent.node.type === PSEUDO_CLASS_TYPE
        && n.node.value === 'host');
      hostQuery.parent().replace(() => ({value: host}));

      return stringify($().get(0));
    }

    return content;
  }
}
