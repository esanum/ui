![pipleline](https://gitlab.com/esanum/ui/badges/main/pipeline.svg)
# Quality Angular UI components kit
Rich & fully native components collection for your beautiful 🤩 angular apps! 

See more demos here 👉 https://ui.esanum.de/

## Killer features
* 60 native components - no third parties
* separated styles from components with themes support
* building your own iconsets
* SASS mixins API for customizing components in the context
* useful builders for each component with code generation
* mobile components adaptation from box

## Compatibility
* Node.js up to v14 LTS Fermium
* Angular 9 or higher 🚀
* Modern browsers and Internet Explorer 11+

## How to install
Install Angular CLI if you have not yet
```
sudo npm i -g @angular/cli
```

Create a new angular project
```bash
ng new esanum-ui-test --style=scss --routing=false
cd esanum-ui-test
```

Install `Esanum UI` package
```bash
npm install @esanum/ui --save-dev
```
Import `Esanum UI` & `Browser Animations` modules into your app module in `src/app/app.module.ts`
```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EsanumUiModule } from '@esanum/ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    EsanumUiModule.forRoot(), 
    BrowserAnimationsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Import styles and assets in `angular.json`
```json
{
  "options": {
    "assets": [
      {
        "glob": "**/*",
        "input": "node_modules/@esanum/ui/lib/assets/icons/animated/",
        "output": "./assets/icons/animated/"
      },
      {
        "glob": "**/*",
        "input": "node_modules/@esanum/ui/lib/assets/icons/svg/",
        "output": "./assets/icons/svg/"
      }
    ],
    "styles": [
      "src/styles.scss"
    ]
  }
}
```

Import the styles into the global app styles `src/styles.scss`
```scss
/* You can add global styles to this file, and also import other style files */
@import "~@esanum/ui/lib/assets/styles/sn-all";
@import "~@esanum/ui/lib/assets/styles/sn-common";
body {
      font-family: $sn-font-family-base;
      font-weight: $sn-font-weight-light;
      font-size: $sn-font-size-normal;
      color: $sn-primary-text-color;
      margin: 20px;
}
```

Bingo 👏 Start your project.
```bash
ng serve
``` 

It is a very fast but rude installation with importing all components & assets.

## How to try
You can go to research all components specifications https://ui.esanum.de/

For fast test add link to `UI` enum in `src/app/app.component.ts`
```typescript
import { Component } from '@angular/core';
import { UI } from '@esanum/ui';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    ui = UI; // link to esanum UI enum
}
```

Add `sn-button` in to your `src/app/app.component.html`

```html
<sn-button text="Click on me"
    [icon]="ui.icons.user">
    <sn-badge [value]="5"></sn-badge>
</sn-button>
```

You should see the button 😎

Problems 🙀? Please, add an issue here 👉 https://gitlab.com/esanum/ui/-/issues

## Polyfills

Include this polyfill for correct library working in IE 11:
* Add file src/polyfills/element-children.js
* Copy this code to src/polyfills/element-children.js
```javascript
(function(constructor) {
    if (constructor && constructor.prototype && constructor.prototype.children == null) {
        Object.defineProperty(constructor.prototype, 'children', {
            get: function() {
                var i = 0,
                    node, nodes = this.childNodes,
                    children = [];
                while (node = nodes[i++]) {
                    if (node.nodeType === 1) {
                        children.push(node);
                    }
                }
                return children;
            }
        });
    }
})(window.Node || window.Element);
```
* Add to your src/polyfills.ts
```typescript
import './polyfills/element-children';
```
