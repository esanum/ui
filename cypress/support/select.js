
export function select(...selector) {
    return selector.join(' ');
};

export function control(name) {
    return `[formControlName=${name}]`;
};
