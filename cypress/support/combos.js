export function allCombos(...sets) {
  const [set] = sets;
  if (sets.length > 1) {
    const combos = [];
    const nested = allCombos(...sets.slice(1));
    for (let e of set) {
      for (let n of nested) {
        combos.push([e, ...n]);
      }
    }
    return combos;
  } else {
    return set.map((e) => [e]);
  }
};

export function randomCombos(count, ...sets) {
  const combos = allCombos(...sets);
  const random = [];
  for (let i = 0; i < count; i++) {
    random.push(combos[Math.ceil(Math.random() * combos.length - 1)]);
  }
  return random;
}
