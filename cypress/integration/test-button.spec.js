import { ButtonBuilder } from './button/button-builder';
import { OUTLINE, SCHEME, SIZE, SPACING, ALL_SCHEMES, ALL_SIZES, ALL_OUTLINES } from '../integration/consts';
import { randomCombos } from '../support/combos';

describe('Button', () => {

  beforeEach(() => {
    cy.visit('/en/handbook/forms/button');
  });

  it('Test scheme', () => {

    const builder = new ButtonBuilder();
    builder.selectScheme(SCHEME.primary);
    builder.checkSnapshot('primary_scheme');

    builder.selectScheme(SCHEME.secondary);
    builder.checkSnapshot('secondary_scheme');

    builder.selectScheme(SCHEME.success);
    builder.checkSnapshot('success_scheme');

    builder.selectScheme(SCHEME.fail);
    builder.checkSnapshot('fail_scheme');

    builder.selectScheme(SCHEME.accent);
    builder.checkSnapshot('accent_scheme');
  });

  it('Test size', () => {

    const builder = new ButtonBuilder();

    builder.selectSize(SIZE.tiny);
    builder.checkSnapshot('tiny_size');

    builder.selectSize(SIZE.small);
    builder.checkSnapshot('small_size');

    builder.selectSize(SIZE.normal);
    builder.checkSnapshot('normal_size');

    builder.selectSize(SIZE.large);
    builder.checkSnapshot('large_size');
  });

  it('Test outline', () => {

    const builder = new ButtonBuilder();

    builder.selectOutline(OUTLINE.fill);
    builder.checkSnapshot('fill_outline');

    builder.selectOutline(OUTLINE.transparent);
    builder.checkSnapshot('transparent_outline');

    builder.selectOutline(OUTLINE.ghost);
    builder.checkSnapshot('ghost_outline');
  });

  it('Test spacing', () => {

    const builder = new ButtonBuilder();

    builder.selectSpacing(SPACING.none);
    builder.checkSnapshot('none_spacing');

    builder.selectSpacing(SPACING.tiny);
    builder.checkSnapshot('tiny_spacing');

    builder.selectSpacing(SPACING.small);
    builder.checkSnapshot('small_spacing');

    builder.selectSpacing(SPACING.normal);
    builder.checkSnapshot('normal_spacing');

    builder.selectSpacing(SPACING.big);
    builder.checkSnapshot('big_spacing');

    builder.selectSpacing(SPACING.large);
    builder.checkSnapshot('large_spacing');

    builder.selectSpacing(SPACING.huge);
    builder.checkSnapshot('huge_spacing');
  });

  it('Pairwase testing', () => {
    const builder = new ButtonBuilder();

    const combos = randomCombos(10, ALL_SCHEMES, ALL_SIZES, ALL_OUTLINES);
    for (let combo of combos) {
      const [scheme, size, outline] = combo;

      builder.selectScheme(scheme);
      builder.selectSize(size);
      builder.selectOutline(outline);
      builder.checkSnapshot([scheme, size, outline].join('_'));

      builder.flush();
    }

  });
});