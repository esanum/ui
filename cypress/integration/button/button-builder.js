import { select } from '../../support/select'
import { BaseBuilder } from "../base-builder";

const BUTTON_BUILDER_HOST = select('app-button-test', 'sn-form');

export class ButtonBuilder extends BaseBuilder {

  constructor() {
    super(BUTTON_BUILDER_HOST, 'button');
  };

  flush() {
    cy.get(this.inputs.scheme.clear).click();
    cy.get(this.inputs.size.clear).click();
    cy.get(this.inputs.outline.clear).click();
    cy.scrollTo(0);
  };
}

