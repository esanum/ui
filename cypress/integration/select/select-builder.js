import { select } from "../../support/select";
import { BaseBuilder } from "../base-builder";

const SELECT_BUILDER_HOST = select('app-select-test', 'sn-form');

export class SelectBuilder extends BaseBuilder {

  constructor() {
    super(SELECT_BUILDER_HOST, 'select');
  }

  flush() {
    cy.get(this.inputs.mode.clear).click();
    cy.get(this.inputs.size.clear).click();
    cy.scrollTo(0);
  };

}
