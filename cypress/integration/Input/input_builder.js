import { BaseBuilder } from "../base-builder";
import { select } from '../../support/select';

const INPUT_BUILDER_HOST = select('app-input-test', 'sn-form');

export class InputBuilder extends BaseBuilder {

    constructor() {
        super(INPUT_BUILDER_HOST, 'input');
    };

    flush() {
        cy.get(this.inputs.type.clear).click();
        cy.get(this.inputs.scheme.clear).click();
        cy.get(this.inputs.size.clear).click();
        cy.scrollTo(0);
    };
} 