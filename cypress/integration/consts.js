export const SCHEME = {
  normal: 'normal',
  failed: 'failed',
  primary: 'primary',
  secondary: 'secondary',
  success: 'success',
  fail: 'fail',
  accent: 'accent'
};

export const ALL_SCHEMES = [
  SCHEME.primary,
  SCHEME.secondary,
  SCHEME.success,
  SCHEME.fail,
  SCHEME.accent
];

export const ALL_INPUT_SCHEMES = [
  SCHEME.normal,
  SCHEME.success,
  SCHEME.failed
];

export const SIZE = {
  tiny: 'tiny',
  small: 'small',
  normal: 'normal',
  large: 'large'
};

export const ALL_SIZES = [
  SIZE.tiny,
  SIZE.small,
  SIZE.normal,
  SIZE.large
];

export const OUTLINE = {
  fill: 'fill',
  transparent: 'transparent',
  ghost: 'ghost'
};

export const ALL_OUTLINES = [
  OUTLINE.fill,
  OUTLINE.transparent,
  OUTLINE.ghost
];

export const SPACING = {

  none: 'none',
  tiny: 'tiny',
  small: 'small',
  normal: 'normal',
  big: 'big',
  large: 'large',
  huge: 'huge'
};

export const ALL_SPACINGS = [
  SPACING.none,
  SPACING.tiny,
  SPACING.small,
  SPACING.normal,
  SPACING.big,
  SPACING.large,
  SPACING.huge
]

export const MODE = {

  single: 'single',
  multiple: 'multiple'
};

export const ALL_MODES = [
  MODE.single,
  MODE.multiple
];

export const ORIENTATION = {
  horizontal: 'horizontal',
  vertical: 'vertical'
};

export const ALIGN = {
  start: 'start',
  center: 'center',
  end: 'end',
  baseline: 'baseline',
  stretch: 'stretch'
};

export const ALL_ALIGNS = [
  ALIGN.start,
  ALIGN.center,
  ALIGN.end,
  ALIGN.baseline,
  ALIGN.stretch
];

export const COLUMNS = {
  two: '2',
  three: '3'
};

export const TYPE = {
  text: 'text',
  number: 'number',
  password: 'password'
};

export const ALL_TYPES = [
  TYPE.text,
  TYPE.number,
  TYPE.password
]