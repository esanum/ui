import { select } from '../../support/select';
import { BaseBuilder } from '../base-builder';

const CHECKBOX_BUILDER_HOST = select('app-checkbox-test', 'sn-form');

export class CheckboxBuilder extends BaseBuilder {

    constructor() {
        super(CHECKBOX_BUILDER_HOST, 'checkbox');
    };
    flush() {
        cy.get(this.inputs.orientation.clear).click();
        cy.get(this.inputs.spacing.clear).click();
        cy.get(this.inputs.size.clear).click();
        cy.get(this.inputs.align.clear).click();
        cy.scrollTo(0);
    };
}