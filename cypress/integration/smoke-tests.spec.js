describe('Smoke tests', () => {

  before(() => {
    cy.visit('/');
  });

  it('Check handbook is online', () => {
    cy.get('h1').should('have.text', 'Quality Angular UI components kit Alpha version')
  });

});
