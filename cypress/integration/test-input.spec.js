import { ALL_INPUT_SCHEMES, ALL_TYPES, SCHEME, SIZE, TYPE } from "./consts";
import { InputBuilder } from './Input/input_builder';
import { randomCombos } from '../support/combos';

describe('Input', () => {

    beforeEach(() => {
        cy.visit('/en/handbook/forms/input');
    });

    it('Test type', () => {
        const builder = new InputBuilder();
        builder.selectType(TYPE.text);
        builder.checkSnapshot('text');

        builder.selectType(TYPE.number);
        builder.checkSnapshot('number');

        builder.selectType(TYPE.password);
        builder.checkSnapshot('password');
    });

    it('Test scheme', () => {
        const builder = new InputBuilder();
        builder.selectScheme(SCHEME.normal);
        builder.checkSnapshot('normal');

        builder.selectScheme(SCHEME.success);
        builder.checkSnapshot('success');

        builder.selectScheme(SCHEME.failed);
        builder.checkSnapshot('failed');
    });

    it('Test size', () => {
        const builder = new InputBuilder();
        builder.selectSize(SIZE.small);
        builder.checkSnapshot('small');

        builder.selectSize(SIZE.normal);
        builder.checkSnapshot('normal');

        builder.selectSize(SIZE.large);
        builder.checkSnapshot('large');
    });

    it('Pairwase testing', () => {
        const builder = new InputBuilder();

        const combos = randomCombos(10, ALL_TYPES, ALL_INPUT_SCHEMES, [SIZE.small, SIZE.normal, SIZE.large]);
        for (let combo of combos) {
            const [type, scheme, size] = combo;

            builder.selectType(type);
            builder.selectScheme(scheme);
            builder.selectSize(size);
            builder.checkSnapshot([type, scheme, size].join('_'));

            builder.flush();
        };
    });
})