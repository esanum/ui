import { CheckboxBuilder } from './checkbox/checkbox-builder';
import { SIZE, ALIGN, COLUMNS, SPACING, ORIENTATION, ALL_SIZES, ALL_ALIGNS, ALL_SPACINGS } from "./consts";
import { randomCombos } from '../support/combos';

describe('Checkbox', () => {

    beforeEach(() => {
        cy.visit('/en/handbook/forms/checkbox');
    });

    it('Test orientation', () => {
        const builder = new CheckboxBuilder();
        builder.selectOrientation(ORIENTATION.horizontal);
        builder.checkSnapshot('horizontal');

        builder.selectOrientation(ORIENTATION.vertical);
        builder.checkSnapshot('vertical');
    });

    it('Test spacing', () => {
        const builder = new CheckboxBuilder();
        builder.selectSpacing(SPACING.none);
        builder.checkSnapshot('none');

        builder.selectSpacing(SPACING.tiny);
        builder.checkSnapshot('spacing_tiny');

        builder.selectSpacing(SPACING.small);
        builder.checkSnapshot('spacing_small');

        builder.selectSpacing(SPACING.normal);
        builder.checkSnapshot('spacing_normal');

        builder.selectSpacing(SPACING.big);
        builder.checkSnapshot('big');

        builder.selectSpacing(SPACING.large);
        builder.checkSnapshot('spacing_large');

        builder.selectSpacing(SPACING.huge);
        builder.checkSnapshot('huge');
    });

    it('Test size', () => {
        const builder = new CheckboxBuilder();
        builder.selectSize(SIZE.tiny);
        builder.checkSnapshot('size_tiny');

        builder.selectSize(SIZE.small);
        builder.checkSnapshot('size_small');

        builder.selectSize(SIZE.normal);
        builder.checkSnapshot('size_normal');

        builder.selectSize(SIZE.large);
        builder.checkSnapshot('size_large');
    });

    it('Test align by vertical', () => {
        const builder = new CheckboxBuilder();
        builder.selectAlign(ALIGN.start);
        builder.checkSnapshot('start');

        builder.selectAlign(ALIGN.center);
        builder.checkSnapshot('center');

        builder.selectAlign(ALIGN.end);
        builder.checkSnapshot('end');

        builder.selectAlign(ALIGN.baseline);
        builder.checkSnapshot('baseline');

        builder.selectAlign(ALIGN.stretch);
        builder.checkSnapshot('stretch');
    });

    it('Test columns', () => {
        const builder = new CheckboxBuilder();
        builder.selectColumns(COLUMNS.two);
        builder.checkSnapshot('2');

        builder.selectColumns(COLUMNS.three);
        builder.checkSnapshot('3');
    });

    it('Pairwase testing', () => {
        const builder = new CheckboxBuilder();

        const combos = randomCombos(10, [ORIENTATION.horizontal, ORIENTATION.vertical], ALL_SPACINGS, ALL_SIZES, ALL_ALIGNS,);
        for (let combo of combos) {
            const [orientation, spacing, size, align,] = combo;

            builder.selectOrientation(orientation);
            builder.selectSpacing(spacing);
            builder.selectSize(size);
            builder.selectAlign(align);
            builder.checkSnapshot([orientation, spacing, size, align,].join('_'));

            builder.flush();
        };
    });
})
