import { SIZE, ALL_MODES, MODE } from "./consts";
import { SelectBuilder } from './select/select-builder';
import { randomCombos } from '../support/combos';


describe('Select', () => {

    beforeEach(() => {
        cy.visit('/en/handbook/forms/select');
    });

    it('Test mode', () => {

        const builder = new SelectBuilder();
        builder.selectMode(MODE.single);
        builder.checkSnapshot('single');

        builder.selectMode(MODE.multiple);
        builder.checkSnapshot('multiple')
    });

    it('Test size', () => {
        const builder = new SelectBuilder();
        builder.selectSize(SIZE.small);
        builder.checkSnapshot('small_size');

        builder.selectSize(SIZE.normal);
        builder.checkSnapshot('normal_size');

        builder.selectSize(SIZE.large);
        builder.checkSnapshot('large_size');
    });

    it('Pairwase testing', () => {
        const builder = new SelectBuilder();

        const combos = randomCombos(10, ALL_MODES, [SIZE.small, SIZE.normal, SIZE.large]);
        for (let combo of combos) {
            const [mode, size] = combo;

            builder.selectMode(mode);
            builder.selectSize(size);
            builder.checkSnapshot([mode, size].join('_'));

            builder.flush();
        }

    });
});