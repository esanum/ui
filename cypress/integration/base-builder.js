import { control, select } from "../support/select";
import { POPOVER_HOST } from "./hosts";

const PREVIEW = select('app-browser-preview', 'div[wrapper]');

export class BaseBuilder {

  host;
  name;

  inputs = {
    scheme: {
      open: select(this.host, control('scheme'), 'button[data-toggle]'),
      clear: select(this.host, control('scheme'), 'button[data-close]')
    },
    size: {
      open: select(this.host, control('size'), 'button[data-toggle]'),
      clear: select(this.host, control('size'), 'button[data-close]')
    },
    outline: {
      open: select(this.host, control('outline'), 'button[data-toggle]'),
      clear: select(this.host, control('outline'), 'button[data-close]')
    },
    spacing: {
      open: select(this.host, control('spacing'), 'button[data-toggle]'),
      clear: select(this.host, control('spacing'), 'button[data-close]')
    },
    mode: {
      open: select(this.host, control('mode'), 'button[data-toggle]'),
      clear: select(this.host, control('mode'), 'button[data-close]')
    },
    orientation: {
      open: select(this.host, control('orientation'), 'button[data-toggle]'),
      clear: select(this.host, control('orientation'), 'button[data-close]')
    },
    align: {
      open: select(this.host, control('align'), 'button[data-toggle]'),
      clear: select(this.host, control('align'), 'button[data-close]')
    },
    columns: {
      open: select(this.host, control('cols'), 'button[data-toggle]'),
      clear: select(this.host, control('cols'), 'button[data-close]')
    },
    type: {
      open: select(this.host, control('type'), 'button[data-toggle]'),
      clear: select(this.host, control('type'), 'button[data-close]')
    },

  };

  constructor(host, name) {
    [this.host, this.name] = [host, name];
  }

  selectScheme(scheme) {
    cy.get(this.inputs.scheme.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(scheme, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectSize(size) {
    cy.get(this.inputs.size.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(size, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectOutline(outline) {
    cy.get(this.inputs.outline.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(outline, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectSpacing(spacing) {
    cy.get(this.inputs.spacing.open).click({ scrollBehavior: true });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(spacing, { matchCase: false })
      .click({ scrollBehavior: true });
  };

  selectMode(mode) {
    cy.get(this.inputs.mode.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(mode, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectOrientation(orientation) {
    cy.get(this.inputs.orientation.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(orientation, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectAlign(align) {
    cy.get(this.inputs.align.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(align, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectColumns(cols) {
    cy.get(this.inputs.columns.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(cols, { matchCase: false })
      .click({ scrollBehavior: false });
  };

  selectType(type) {
    cy.get(this.inputs.type.open).click({ scrollBehavior: false });
    cy.get(select(POPOVER_HOST, 'ul li'))
      .contains(type, { matchCase: false })
      .click({ scrollBehavior: false });
  }

  checkSnapshot(name) {
    cy.get(PREVIEW).matchImageSnapshot([this.name, name].join('_'));
  };

}
