export const ICON_API = {
  'properties': {
    'icon': {'description': 'Icon query in special format', 'type': 'name:type:iconset'},
    'stroke': {
      'description': 'Stroke for icon',
      'type': 'ui.stroke.',
      'options': ['thin', 'normal', 'bold'],
      'default': 'normal'
    },
    'size': {
      'description': 'Size for icon',
      'type': 'ui.size.',
      'options': ['tiny', 'small', 'normal', 'large'],
      'default': 'normal'
    },
    'color': {'description': 'Color for icon', 'type': 'string', 'default': '#444444'},
    'modifier': {
      'description': 'Modifier for icon: add, edit, delete',
      'path': 'ui.icon.modifier',
      'options': ['add', 'edit', 'delete']
    }
  }
};
