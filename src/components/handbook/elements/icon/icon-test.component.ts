import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { ICON_API } from './icon-test.api';

@Component({
  selector: 'app-icon-test',
  templateUrl: './icon-test.component.html',
  styleUrls: ['./icon-test.component.scss']
})
export class IconTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    icon: ICON_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/elements/icon';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=32%3A95';

  @ViewChild('tabs') tabs: TabsComponent;

  icons = [];

  iconControl = this.fb.control({
    path: 'icons',
    name: 'runningMan',
    value: UI.icons.animated.runningMan
  });

  sizeControl = this.fb.control(null);
  strokeControl = this.fb.control(null);
  modifierControl = this.fb.control(null);
  colorControl = this.fb.control(UI.color.gray);
  builder = this.fb.group({
    icon: this.iconControl,
    size: this.sizeControl,
    stroke: this.strokeControl,
    modifier: this.modifierControl,
    color: this.colorControl
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.strokeControl.disable();

    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.iconControl.valueChanges.subscribe(value => {
      ['animated', 'apps', 'flags'].some(type => value.value.includes(type))
        ? this.strokeControl.disable()
        : this.strokeControl.enable();
    });
  }
}
