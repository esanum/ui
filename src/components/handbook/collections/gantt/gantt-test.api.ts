export const GANTT_API = {
  'properties': {
    'type': {'description': 'Type of gantt', 'type': 'ui.gantt.type.month | ui.gantt.type.year', 'default': 'month'},
    'width': {'description': 'Card width', 'path': 'ui.width', 'default': 'fluid', 'options': ['default', 'fluid']},
    'title': {'description': 'Title', 'type': 'string', 'default': 'Test title'},
    'loading': {'description': 'Loading', 'type': 'boolean', 'default': 'false'}
  },
  'content': {
    'toolsTemplate': {'selector': '#ganttToolsTemplate', 'description': 'Tools template'},
    'titleTemplate': {'selector': '#ganttTitleTemplate', 'description': 'title template'}
  }
};

export const GANTT_LINE_API = {
  'properties': {
    'title': {'description': 'Title', 'type': 'string', 'default': 'Test title'}
  },
  'content': {
    'titleTemplate': {'selector': '#ganttLineTitleTemplate', 'description': 'title template'}
  }
};
