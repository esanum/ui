export const TABLE_API = {
  'properties': {
    'features': {
      'description': 'Table features',
      'path': 'ui.feature',
      'options': ['search', 'reload']
    },
    'fetcher': {'description': 'Table fetch function', 'type': 'Function'},
    'reloaded': {'description': 'Output event of reload table'}
  },
  'methods': {
    'load': {'description': 'reload table'},
    'sorting': {'description': 'sorting data table by field'}
  },
  'content': {
    'rowActionsTemplate': {'selector': '#tableRowActionsTemplate', 'description': 'table row actions template'},
    'actionsTemplate': {'selector': '#tableActionsTemplate', 'description': 'table actions template'},
    'filtersTemplate': {'selector': '#tableFiltersTemplate', 'description': 'table filters template'}
  }
};

export const COLUMN_API = {
  'properties': {
    'title': {'description': 'Column title', 'type': 'string'},
    'width': {'description': 'Column width', 'type': 'string'},
    'align': {'description': 'Column title align', 'type': 'string', 'path': 'ui.text.align'},
    'orderBy': {'description': 'Column sort field', 'type': 'string'}
  },
  'content': {
    'tableCellTemplate': {'selector': '#tableCellTemplate', 'description': 'table cell template'}
  }
};
