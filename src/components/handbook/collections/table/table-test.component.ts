import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { serialize } from '@junte/serialize-ts';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { TableSections, TableState, TableStateUpdate } from './data/table-data.types';
import { COLUMN_API, TABLE_API } from './table-test.api';

const DEFAULT_FIRST = 10;

@Component({
  selector: 'app-table-test',
  templateUrl: './table-test.component.html',
  styleUrls: ['./table-test.component.scss']
})
export class TableTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  sections = TableSections;
  api = {
    table: TABLE_API,
    column: COLUMN_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/collections/table';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=1270%3A541';

  @ViewChild('tabs') tabs: TabsComponent;

  searchControl = this.fb.control(true);
  reloadControl = this.fb.control(true);
  filterControl = this.fb.control(true);
  actionsControl = this.fb.control(true);
  rowActionsControl = this.fb.control(true);

  builder = this.fb.group({
    search: this.searchControl,
    reload: this.reloadControl,
    filter: this.filterControl,
    actions: this.actionsControl,
    rowActions: this.rowActionsControl
  });

  state: TableState;

  constructor(private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(({first, offset, ability, q}) => {
      this.state = {
        first: !!+first && +first !== DEFAULT_FIRST ? +first : undefined,
        offset: +offset > 0 ? +offset : 0,
        q: q || null,
        ability: ability || null
      };
    });

    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

  save(state: TableStateUpdate) {
    this.router.navigate([serialize(state)], {relativeTo: this.route})
      .then(() => null);
  }
}
