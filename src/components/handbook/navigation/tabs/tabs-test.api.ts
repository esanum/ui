export const TABS_API = {
  'properties': {
    'features': {
      'description': 'Adapted tabs on mobile view',
      'path': 'ui.feature',
      'options': ['adapted']
    }
  }
};

export const TAB_API = {
  'properties': {
    'title': {'description': 'Title of tab', 'type': 'string'},
    'titleTemplate': {'description': 'Title template of tab', 'type': 'TemplateRef<any>'},
    'icon': {'description': 'Icon for tab', 'type': 'string'}
  }
};
