export const PAGER_API = {
  'properties': {
    'size': {
      'description': 'Pager size',
      'type': 'number'
    },
    'count': {
      'description': 'Items count for pager',
      'type': 'number'
    },
    'pageSize': {
      'description': 'Page size for pager',
      'type': 'number',
      'default': '10'
    },
    'mode': {
      'description': 'Mode for pager',
      'path': 'ui.pager.mode',
      'options': ['offset', 'page'],
      'default': 'offset'
    },
    'link': {
      'description': 'Link to page. %page% pattern is replaced by page number',
      'type': 'string',
      'default': '?page=%page%'
    },
    'attributes': {
      'description': 'Callback that accept string parameter to set custom attributes to links',
      'type': 'function'
    }
  }
};
