export const ACCORDION_API = {
  'properties': {
    'active': {'description': 'Accordion active section', 'type': 'array of numbers', 'default': '[]'},
    'behaviour': {
      'description': 'Simultaneously opened sections',
      'path': 'ui.behaviour',
      'options': ['single', 'multiple'],
      'default': 'single'
    },
    'state': {
      'description': 'Accordion outline',
      'path': 'ui.outline',
      'options': ['ghost', 'fill'],
      'default': 'ghost'
    }
  }
};

export const ACCORDION_SECTION_API = {
  'properties': {
    'icon': {'description': 'Accordion section icon', 'type': 'string'},
    'state': {'description': 'State of accordion', 'path': 'ui.state', 'options': ['warning', 'loading']},
    'title': {'description': 'Accordion section title', 'type': 'string'}
  },
  'content': {
    'contentTemplate': {'selector': '#accordionContentTemplate', 'description': 'Accordion section content template'},
    'titleTemplate': {'selector': '#accordionTitleTemplate', 'description': 'Accordion section title template'}
  }
};
