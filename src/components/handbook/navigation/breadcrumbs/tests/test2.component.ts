import { Component } from '@angular/core';
import { UI } from 'esanum-ui';

@Component({
  selector: 'app-breadcrumbs-test2',
  template: `
    <sn-stack [orientation]="ui.orientation.horizontal" [align]="ui.align.center">
      <sn-icon [icon]="ui.icons.chevronRight"></sn-icon>
      <div block>2</div>
    <router-outlet></router-outlet>
    </sn-stack>
  `,
  styleUrls: ['./test.component.scss']
})

export class BreadCrumbTest2Component {
  ui = UI;
}
