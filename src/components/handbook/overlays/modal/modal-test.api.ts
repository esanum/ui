export const MODAL_API = {
  'methods': {
    'open': {'description': 'show modal'},
    'close': {'description': 'close modal'}
  }
};
