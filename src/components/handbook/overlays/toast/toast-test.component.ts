import { Component, ComponentFactoryResolver, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { ModalTestFactoryComponent } from 'src/components/handbook/overlays/modal/test.component';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { ToastService } from '../../../../../projects/esanum-ui/src/lib/overlays/toast/toast.service';
import { ToastOptions } from '../../../../../projects/esanum-ui/src/lib/overlays/toast/toast.type';

@Component({
  selector: 'app-toast-test',
  templateUrl: './toast-test.component.html'
})
export class ToastTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/overlays/toast';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=56%3A9';

  schemeControl = this.fb.control(UI.scheme.primary);
  sizeControl = this.fb.control(UI.size.small);
  titleControl = this.fb.control(false);
  iconControl = this.fb.control(false);
  closeControl = this.fb.control(5000);
  contentControl = this.fb.control(false);
  builder = this.fb.group({
    scheme: this.schemeControl,
    size: this.sizeControl,
    title: this.titleControl,
    icon: this.iconControl,
    close: this.closeControl,
    content: this.contentControl
  });

  @ViewChild('tabs') tabs: TabsComponent;

  constructor(private fb: FormBuilder,
              private toastService: ToastService,
              private injector: Injector,
              private cfr: ComponentFactoryResolver) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.contentControl.valueChanges
      .subscribe(value => {
        if (value) {
          this.sizeControl.disable();
          this.titleControl.disable();
          this.titleControl.setValue(false);
          this.iconControl.disable();
          this.iconControl.setValue(false);
        } else {
          this.sizeControl.enable();
          this.titleControl.enable();
          this.iconControl.enable();
        }
      });
  }

  showToast() {
    const component = this.cfr.resolveComponentFactory(ModalTestFactoryComponent).create(this.injector);
    const options = new ToastOptions({
      title: this.titleControl.value ? 'Praesent lobortis' : null,
      message: 'Praesent lobortis nisi cursus tortor tristique tincidunt.',
      icon: this.iconControl.value ? UI.icons.rocket : null,
      content: this.contentControl.value ? component : null,
      scheme: this.schemeControl.value,
      size: this.sizeControl.value,
      closeDelay: this.closeControl.value,
    });

    this.toastService.show(options);
  }
}
