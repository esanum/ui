import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  AccordionModule,
  AppLayoutModule,
  ButtonModule,
  CheckboxModule,
  FormModule,
  GridModule,
  InputModule,
  LinkModule,
  SelectModule,
  StackModule,
  TabsModule,
  ToastModule
} from 'esanum-ui';
import { SharedModule } from '../../shared/shared.module';
import { ToastTestComponent } from './toast-test.component';

@NgModule({
  imports: [
    AppLayoutModule,
    ButtonModule,
    CommonModule,
    GridModule,
    LinkModule,
    SharedModule,
    StackModule,
    TabsModule,
    ToastModule,
    ButtonModule,
    FormModule,
    ReactiveFormsModule,
    SelectModule,
    CheckboxModule,
    AccordionModule,
    InputModule
  ],
  exports: [
    ToastTestComponent
  ],
  declarations: [
    ToastTestComponent
  ]
})
export class ToastTestModule {
}
