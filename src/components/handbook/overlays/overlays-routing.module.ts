import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModalTestComponent } from './modal/modal-test.component';
import { OverlaysTestComponent } from './overlays-test.component';
import { PopoverTestComponent } from './popover/popover-test.component';
import { ToastTestComponent } from './toast/toast-test.component';

export const routes: Routes = [
  {
    path: '',
    data: {breadcrumb: 'Overlays'},
    children: [
      {
        path: '',
        component: OverlaysTestComponent,
      },
      {
        path: 'modal',
        component: ModalTestComponent,
        data: {breadcrumb: 'Modal', animation: 'Modal'}
      },
      {
        path: 'popover',
        component: PopoverTestComponent,
        data: {breadcrumb: 'Popover', animation: 'Popover'}
      },
      {
        path: 'toast',
        component: ToastTestComponent,
        data: {breadcrumb: 'Toast', animation: 'Toast'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverlaysRoutingModule {
}
