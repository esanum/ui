export const IMAGE_UPLOADER_API = {
  'properties': {
    'uploader': {'description': 'Upload function', 'type': 'Function'},
    'image': {'description': 'Image', 'type': 'string', 'default': ''},
    'valueField': {'description': 'Value field', 'type': 'string', 'default': ''},
    'urlField': {'description': 'Url field', 'type': 'string', 'default': ''},
    'shape': {'description': 'Avatar shape', 'path': 'ui.shape', 'default': 'circle', 'options': ['circle', 'square']},
    'width': {'description': 'Width of uploader', 'type': 'number', 'default': 200},
    'height': {'description': 'Width of uploader', 'type': 'number', 'default': 200},
    'area': {'description': 'Size of crop area', 'type': 'CropperPosition', 'default': '{width: 200, height: 200}'},
    'min': {'description': 'Min of cropping', 'type': 'number', 'default': '0.01'},
    'max': {'description': 'Max of cropping', 'type': 'number', 'default': '5'},
    'step': {'description': 'Step of cropping', 'type': 'number', 'default': '0.01'}
  }
};
