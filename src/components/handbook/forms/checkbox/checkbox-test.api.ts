export const CHECKBOX_API = {
  'properties': {
    'label': {'description': 'Label name for checkbox', 'type': 'string'},
    'size': {'description': 'Size for checkbox', 'path': 'ui.size', 'options': ['tiny', 'small', 'normal', 'large'], 'default': 'normal'},
    'align': {
      'description': 'Align by vertical for checkbox',
      'path': 'ui.align',
      'options': ['center', 'start', 'end'],
      'default': 'center'
    },
    'value': {'description': 'Value for checkbox', 'type': 'any'}
  }
};

export const CHECKBOX_GROUP_API = {
  'properties': {
    'orientation': {
      'description': 'Defined main axis of elements align',
      'path': 'ui.orientation',
      'default': 'vertical',
      'options': ['vertical', 'horizontal']
    },
    'align': {'description': 'Align in radio group', 'path': 'ui.align'},
    'cols': {'description': 'Count of cols in checkbox group', 'type': 'number'},
    'size': {
      'description': 'Size for checkbox in checkbox group',
      'path': 'ui.size',
      'options': ['tiny', 'small', 'normal', 'large'],
      'default': 'normal'
    },
    'spacing': {
      'description': 'Spacing between radio item',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge'],
      'default': 'small'
    },
    'features': {'description': 'Adapted radio group on mobile view', 'path': 'ui.feature', 'options': ['adapted']}
  }
};
