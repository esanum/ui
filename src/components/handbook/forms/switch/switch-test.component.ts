import { KeyValue } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BreakpointService, TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK, HEROES } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';
import { SWITCH_API, SWITCH_GROUP_API } from './switch-test.api';

enum SwitchType {
  single,
  group
}

@Component({
  selector: 'app-switch-test',
  templateUrl: './switch-test.component.html',
  styleUrls: ['./switch-test.component.scss']
})
export class SwitchTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    switch: SWITCH_API,
    switchGroup: SWITCH_GROUP_API
  };
  handbook = HANDBOOK;
  heroes = HEROES;
  language = Language;
  switchType = SwitchType;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/switch';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=2570%3A2784';

  @ViewChild('tabs') tabs: TabsComponent;

  typeControl = this.fb.control(SwitchType.single);
  styleControl = this.fb.control(null);
  sizeControl = this.fb.control(null);
  alignControl = this.fb.control(null);
  labelControl = this.fb.control(true);
  customLabelControl = this.fb.control(false);
  tagsControl = this.fb.control({value: false, disabled: true});
  iconsControl = this.fb.control(false);
  disabledControl = this.fb.control(false);
  orientationControl = this.fb.control(null);
  spacingControl = this.fb.control(null);
  colsControl = this.fb.control(null);

  builder = this.fb.group({
    type: this.typeControl,
    style: this.styleControl,
    size: this.sizeControl,
    align: this.alignControl,
    label: this.labelControl,
    customLabel: this.customLabelControl,
    tags: this.tagsControl,
    icons: this.iconsControl,
    disabled: this.disabledControl,
    orientation: this.orientationControl,
    spacing: this.spacingControl,
    cols: this.colsControl
  });

  switchControl = this.fb.control(false);
  heroControl = this.fb.control([this.heroes.captain.code], Validators.required);

  form = this.fb.group({
    switch: this.switchControl,
    hero: this.heroControl
  });

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => 0;

  constructor(private fb: FormBuilder,
              public breakpoint: BreakpointService) {
  }

  ngOnInit() {
    this.typeControl.valueChanges.subscribe(() => {
      this.orientationControl.setValue(null);
      this.colsControl.setValue(null);
      this.disabledControl.setValue(false);
    });

    this.orientationControl.valueChanges.subscribe((value) => {
      if (value === UI.orientation.horizontal) {
        this.colsControl.setValue(null);
        this.colsControl.disable({emitEvent: false});
      } else {
        this.colsControl.enable({emitEvent: false});
      }
    });

    this.styleControl.valueChanges.subscribe((value) => {
      if (value === UI.switch.style.default) {
        this.tagsControl.enable({emitEvent: false});
        this.iconsControl.setValue(false);
        this.iconsControl.disable({emitEvent: false});
      } else {
        this.tagsControl.setValue(false);
        this.tagsControl.disable({emitEvent: false});
        this.iconsControl.enable({emitEvent: false});
      }
    });

    this.labelControl.valueChanges.subscribe((value) => {
      if (value) {
        this.customLabelControl.enable({emitEvent: false});
      } else {
        this.customLabelControl.setValue(false);
        this.customLabelControl.disable({emitEvent: false});
      }
    });

    this.disabledControl.valueChanges.subscribe((disabled) => {
      if (disabled) {
        (this.typeControl.value === SwitchType.single)
          ? this.switchControl.disable({emitEvent: false})
          : this.heroControl.disable({emitEvent: false});
      } else {
        (this.typeControl.value === SwitchType.single)
          ? this.switchControl.enable({emitEvent: false})
          : this.heroControl.enable({emitEvent: false});
      }
    });

    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }
}
