export const INPUT_API = {
  'properties': {
    'icon': {'description': 'Icon for input', 'type': 'string'},
    'label': {'description': 'Label for input', 'type': 'string'},
    'name': {'description': 'Name for input', 'type': 'string'},
    'transform': {
      'description': 'Text transform for input',
      'path': 'ui.text.transform',
      'options': ['capitalize', 'uppercase', 'lowercase', 'ranks']
    },
    'autocomplete': {'description': 'Auto complete for input', 'path': 'ui.input.autocomplete', 'options': ['on', 'off']},
    'textAlign': {'description': 'Input text align', 'path': 'ui.text.align', 'default': 'left', 'options': ['left', 'right']},
    'min': {'description': 'Minimum number value that can be entered. For input with typeControl = number', 'type': 'number'},
    'max': {'description': 'Maximum number value that can be entered. For input with typeControl = number', 'type': 'number'},
    'step': {'description': 'Step for entered value. For input with typeControl = number', 'type': 'number'},
    'readonly': {'description': 'Used to specify that the input field is read-only', 'type': 'boolean', 'default': 'false'},
    'scheme': {'description': 'Input scheme', 'path': 'ui.input.scheme', 'default': 'normal', 'options': ['normal', 'success', 'failed']},
    'placeholder': {'description': 'Input placeholder', 'type': 'string'},
    'type': {'description': 'Input typeControl', 'path': 'ui.input.type', 'default': 'text', 'options': ['text', 'number', 'password']},
    'size': {'description': 'Input size', 'path': 'ui.size', 'default': 'normal', 'options': ['small', 'normal', 'large']},
    'width': {'description': 'Input width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'state': {'description': 'Input state', 'path': 'ui.state', 'options': ['loading', 'warning', 'checked']},
    'rows': {'description': 'Max rows for multiline mode', 'type': 'number', 'default': 5},
    'mask': {'description': 'Mask pattern where _ - is digit', 'type': 'string', 'default': null},
    'features': {
      'description': 'Button for reset input; Allow multiple lines in input; Copy button',
      'path': 'ui.feature',
      'options': ['allowEmpty', 'multiline', 'copy']
    },
    'click': {'description': 'Click event', 'path': 'EventEmitter'}
  }
};
