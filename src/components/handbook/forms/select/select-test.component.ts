import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { Observable, of } from 'rxjs';
import { debounceTime, delay, finalize, tap } from 'rxjs/operators';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';
import { SELECT_API, SELECT_OPTION_API } from './select-test.api';

@Component({
  selector: 'app-select-test',
  templateUrl: './select-test.component.html',
  styleUrls: ['./select-test.component.scss']
})
export class SelectTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  api = {
    select: SELECT_API,
    selectOption: SELECT_OPTION_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/select';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=114%3A219';

  @ViewChild('tabs') tabs: TabsComponent;

  selectIcon = UI.icons.user;
  selectLabel = 'Hero:';
  selected = {heroes: []};
  created = {heroes: []};

  modeControl = this.fb.control(null);
  sizeControl = this.fb.control(null);
  placementControl = this.fb.control(null);
  disabledControl = this.fb.control(false);
  allowEmptyControl = this.fb.control(false);
  searchControl = this.fb.control(false);
  loaderControl = this.fb.control(null);
  optionTemplateControl = this.fb.control(false);
  labelControl = this.fb.control(false);
  iconControl = this.fb.control(false);
  loadingControl = this.fb.control(false);
  placeholderControl = this.fb.control(true);
  inlineControl = this.fb.control(false);
  headerTemplateControl = this.fb.control(false);
  emptyTemplateControl = this.fb.control(false);
  groupTemplateControl = this.fb.control(false);

  builder = this.fb.group({
    mode: this.modeControl,
    size: this.sizeControl,
    placement: this.placementControl,
    disabled: this.disabledControl,
    allowEmpty: this.allowEmptyControl,
    search: this.searchControl,
    loader: this.loaderControl,
    optionTemplate: this.optionTemplateControl,
    label: this.labelControl,
    icon: this.iconControl,
    loading: this.loadingControl,
    placeholder: this.placeholderControl,
    inline: this.inlineControl,
    headerTemplate: this.headerTemplateControl,
    emptyTemplate: this.emptyTemplateControl,
    groupTemplate: this.groupTemplateControl
  });

  selectControl = this.fb.control({value: null, disabled: this.disabledControl.value});
  form = this.fb.group({
    select: this.selectControl
  });

  heroes = [
    {id: 1, name: 'Ironman', avatar: 'assets/images/heroes/ironman.svg', likes: 381, universe: {id: 1, name: 'Marvel'}},
    {id: 2, name: 'Hulk', avatar: 'assets/images/heroes/hulk.svg', likes: 412, universe: {id: 1, name: 'Marvel'}},
    {id: 3, name: 'Batman', avatar: 'assets/images/heroes/batman.svg', likes: 221, universe: {id: 2, name: 'DC'}},
    {id: 4, name: 'Wonder Woman', avatar: 'assets/images/heroes/wonder.svg', likes: 739, universe: {id: 2, name: 'DC'}}
  ];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.disabledControl.valueChanges
      .subscribe(disabled => disabled
        ? this.selectControl.disable({emitEvent: false})
        : this.selectControl.enable({emitEvent: false}));

    this.labelControl.valueChanges
      .subscribe(value => {
        if (value) {
          this.iconControl.setValue(false);
          this.iconControl.disable({emitEvent: false});
        } else {
          this.iconControl.enable({emitEvent: false});
        }
      });

    this.modeControl.valueChanges
      .pipe(debounceTime(500))
      .subscribe(mode => {
        if (mode === UI.select.mode.multiple) {
          this.selectIcon = UI.icons.team;
          this.selectLabel = 'Heroes:';
          this.selectControl.setValue([]);
        } else {
          this.selectIcon = UI.icons.user;
          this.selectLabel = 'Hero:';
          this.selectControl.setValue(null);
        }
      });
  }

  trackHero(index, hero: { id: number }) {
    return hero.id;
  }

  search() {
    return (query: string) => new Observable(observable => {
      observable.next(this.heroes.filter(hero => !query
        || hero.name.toLowerCase().includes(query.toLowerCase())));
      observable.complete();
    }).pipe(
      tap(() => this.loadingControl.patchValue(true)),
      finalize(() => this.loadingControl.patchValue(false)),
      delay(2000));
  }

  createHero(name: string, close: Function): Observable<null> {
    const id = this.created.heroes.length + 100;
    this.created.heroes.push({id, name});
    const selected = this.selectControl.value;
    selected.push(id);
    this.selectControl.setValue(selected);
    close();
    return of(null);
  }
}
