export const SLIDER_API = {
  'properties': {
    'min': {'description': 'Slider min', 'type': 'number', 'default': 0},
    'max': {'description': 'Slider max', 'type': 'number', 'default': 100},
    'step': {'description': 'Slider step', 'type': 'number', 'default': 1}
  }
};
