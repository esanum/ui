export const FORM_API = {
  'properties': {
    'form': {'description': 'Name form group', 'type': 'FormGroup'},
    'title': {'description': 'Title for form', 'type': 'string'},
    'state': {'description': 'State of form', 'path': 'ui.state', 'options': ['error', 'loading']},
    'height': {'description': 'Height of form', 'path': 'ui.height', 'options': ['default', 'fluid'], 'default': 'default'}
  },
  'methods': {
    'success': {'description': 'show success animation'}
  }
};

export const FORM_CONTROL_API = {
  'properties': {
    'name': {'description': 'Form control name', 'type': 'string'}
  }
};

export const FORM_LABEL_API = {
  'properties': {
    'for': {'description': 'The identifier of the item to associate with', 'type': 'string'}
  }
};

export const FORM_MESSAGE_API = {
  'properties': {
    'validator': {
      'description': 'Validation type error',
      'path': 'ui.validator',
      'options': ['required', 'minlength', 'min', 'max']
    }
  }
};

export const FORM_ITEM_API = {
  'properties': {
    'orientation': {
      'description': 'Form item orientation',
      'path': 'ui.orientation',
      'default': 'vertical',
      'options': ['horizontal', 'vertical']
    },
    'align': {
      'description': 'Align of elements in form item',
      'path': 'ui.align',
      'default': 'stretch',
      'options': ['start', 'center', 'end', 'baseline', 'stretch']
    },
    'gutter': {
      'description': 'Space between elements in form item',
      'path': 'ui.gutter',
      'default': 'tiny',
      'options': ['tiny', 'small', 'normal', 'big', 'large', 'huge']
    }
  }
};
