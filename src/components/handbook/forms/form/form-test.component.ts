import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormComponent, TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';
import { FORM_API, FORM_CONTROL_API, FORM_ITEM_API, FORM_LABEL_API, FORM_MESSAGE_API } from './form-test.api';

enum Gender {
  man = 'man',
  woman = 'woman'
}

@Component({
  selector: 'app-form-test',
  templateUrl: './form-test.component.html',
  styleUrls: ['./form-test.component.scss']
})

export class FormTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    form: FORM_API,
    formControl: FORM_CONTROL_API,
    formLabel: FORM_LABEL_API,
    formMessage: FORM_MESSAGE_API,
    formItem: FORM_ITEM_API
  };
  handbook = HANDBOOK;
  language = Language;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/form';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=1291%3A2727';

  gender = Gender;
  countries: string[] = ['Russia', 'Australia', 'Austria', 'Brazil', 'Germany', 'Latvia', 'Monaco', 'Ukraine'];
  pets: string[] = ['cat', 'dog', 'fish', 'parrot'];
  status: string[] = ['married', 'not married', 'is actively looking'];

  @ViewChild('tabs') tabs: TabsComponent;

  @ViewChild('formTest')
  formTest: FormComponent;

  titleControl = this.fb.control(true);
  stateControl = this.fb.control(null);

  builder = this.fb.group({
    title: this.titleControl,
    state: this.stateControl
  });

  errors = [];

  form = this.fb.group({
    personals: this.fb.group({
      firstName: this.fb.control(null, [Validators.required, Validators.minLength(3)]),
      lastName: this.fb.control(null),
    }),
    email: this.fb.control(null),
    password: this.fb.control(null, [Validators.required, Validators.minLength(5)]),
    phone: this.fb.control(null),
    gender: this.fb.control(Gender.man),
    birthday: this.fb.control(new Date()),
    country: this.fb.control(this.countries[0]),
    notification: this.fb.control(true),
    pets: this.fb.control(null),
    status: this.fb.control(this.status[0])
  });

  submitted = '';

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

  submit() {
    this.submitted = JSON.stringify(this.form.value, null, 2);
  }
}
