export const SELECTABLE_API = {
  'properties': {
    'configure': {
      'description': 'Selectable configuration',
      'type': '{mode?: SelectMode, value: any, enabled?: boolean, features?: Feature[]}',
      'default': '{}'
    }
  }
};
