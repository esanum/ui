export const CALENDAR_API = {
  'properties': {
    'features': {'description': 'Calendar features', 'path': 'ui.feature', 'options': ['today']},
    'disabled': {'description': 'Set disabled state', 'type': 'boolean', 'default': 'false'},
    'readonly': {'description': 'Set readonly state', 'type': 'boolean', 'default': 'false'},
    'period': {'description': 'Set current month for displaying', 'type': 'Date'}
  }
};
