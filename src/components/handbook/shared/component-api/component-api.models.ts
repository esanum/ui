export class PropertyMetadata {
  name?: string;
  description: string;
  path?: string;
  type?: string;
  options?: string[];
  default?: string | number;
}

export class MethodMetadata {
  description: string;
}

export class ContentMetadata {
  selector: string;
  description: string;
}
