import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StackModule, TextPipesModule } from 'esanum-ui';
import { CodeHighlightModule } from '../code-highlight/code-highlight.module';
import { ComponentApiComponent } from './component-api.component';

@NgModule({
  imports: [
    CommonModule,
    CodeHighlightModule,
    TextPipesModule,
    StackModule
  ],
  exports: [
    ComponentApiComponent
  ],
  declarations: [
    ComponentApiComponent
  ]
})
export class ComponentApiComponentModule {
}
