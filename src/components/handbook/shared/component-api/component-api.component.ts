import { Component, Input } from '@angular/core';
import { UI } from 'esanum-ui';
import { ContentMetadata, MethodMetadata, PropertyMetadata } from './component-api.models';
import { Language } from '../code-highlight/enum';
import { SelectorType } from './enums';


@Component({
  selector: 'app-component-api',
  templateUrl: './component-api.component.html',
  styleUrls: ['./component-api.component.scss']
})
export class ComponentApiComponent {

  ui = UI;
  language = Language;
  selectorType = SelectorType;

  properties: { [key: string]: PropertyMetadata };
  methods: { [key: string]: MethodMetadata };
  content: { [key: string]: ContentMetadata };

  @Input()
  selector: string;

  @Input()
  type = SelectorType.Component;

  @Input()
  api: {
    properties?: { [key: string]: PropertyMetadata };
    methods?: { [key: string]: MethodMetadata };
    content?: { [key: string]: ContentMetadata };
  };
}
