import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AccordionModule,
  AppLayoutModule,
  DynamicModule,
  FormModule,
  GridModule,
  IconModule,
  LinkModule,
  StackModule,
  TabsModule
} from 'esanum-ui';
import { InPaintTestComponent } from './in-paint-test.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IconModule,
    LinkModule,
    StackModule,
    TabsModule,
    GridModule,
    FormModule,
    SharedModule,
    AppLayoutModule,
    DynamicModule,
    AccordionModule
  ],
  exports: [
    InPaintTestComponent
  ],
  declarations: [
    InPaintTestComponent
  ],
})
export class InPaintTestModule {
}

