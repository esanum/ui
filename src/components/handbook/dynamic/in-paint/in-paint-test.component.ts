import { Component } from '@angular/core';
import { UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';

@Component({
  selector: 'app-in-paint-test',
  templateUrl: './in-paint-test.component.html',
  styleUrls: ['./in-paint-test.component.scss']
})
export class InPaintTestComponent {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/dynamic/in-paint';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=2616%3A70';
}
