export const CHART_API = {
  'properties': {
    'title': {'description': 'Title of the charts group', 'type': 'string'},
    'metric': {'description': 'Name of metric for the charts', 'type': 'string'},
    'state': {'description': 'Chart state', 'path': 'ui.state', 'options': ['loading']}
  }
};

export const CHART_INDICATOR_API = {
  'properties': {
    'label': {'description': 'Label name of chart item indicator', 'type': 'string'},
    'value': {'description': 'Value of chart item indicator', 'type': 'number'},
    'title': {'description': 'Title name of chart item indicator', 'type': 'string'},
    'color': {
      'description': 'Set the color to \'red\' | \'green\' | \'blue\' or other custom colors (css color) for chart item',
      'type': 'string | Color'
    },
    'data': {'description': 'Data of chart item indicator', 'type': 'any'}
  }
};

