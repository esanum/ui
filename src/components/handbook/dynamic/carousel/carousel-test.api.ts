export const CAROUSEL_API = {
  'properties': {
    'animationSpeed': {
      'description': 'Speed of carousel items changing animation', 'type': 'number', 'default': '500'
    },
    'orientation': {
      'description': 'Orientation of autoplay carousel',
      'path': 'ui.carousel.orientation',
      'options': ['left', 'right'],
      'default': 'left'
    },
    'autoplay': {'description': 'Carousel autoplay', 'type': 'boolean', 'default': 'false'},
    'autoplaySpeed': {'description': 'Speed of carousel autoplay', 'type': 'number', 'default': '1500'},
    'animation': {
      'description': 'Type of animation',
      'path': 'ui.carousel.animation',
      'options': ['slide', 'fade'],
      'default': 'slide'
    },
    'infinite': {'description': 'Carousel infinite', 'type': 'boolean', 'default': 'true'},
    'dots': {'description': 'Carousel dots', 'type': 'boolean', 'default': 'false'},
    'arrows': {'description': 'Carousel arrows', 'type': 'boolean', 'default': 'true'}
  },
  'content': {
    'items': {'selector': 'sn-carousel-item', 'description': 'Carousel items'}
  }
};
