export const DATE_PERIOD_API = {
  'properties': {
    'start': {'description': 'Start date of period', 'type': 'Date', 'default': 'new Date()'},
    'end': {'description': 'End date of period', 'type': 'Date', 'default': 'new Date()'},
    'current': {'description': 'Current date', 'type': 'Date'}
  }
};

