export const TIMER_API = {
  'properties': {
    'days': {'description': 'Days count', 'type': 'number'},
    'hours': {'description': 'Hours count', 'type': 'number'},
    'minutes': {'description': 'Minutes count', 'type': 'number'},
    'seconds': {'description': 'Seconds count', 'type': 'number'}
  },
  'methods': {
    'pause': {'description': 'Pause timer'},
    'reset': {'description': 'Reset timer'}
  }
};

