export const BAR_API = {
  'content': {
    'circleBarContentTemplate': {
      'selector': '#circleBarContentTemplate',
      'description': 'Circle bar content template'
    }
  }
};

export const BAR_INDICATOR_API = {
  'properties': {
    'value': {'description': 'Indicator value', 'type': 'number'},
    'title': {'description': 'Indicator title', 'type': 'string'},
    'color': {'description': 'Indicator color', 'type': 'string'},
    'width': {'description': 'Indicator width', 'type': 'string'}
  }
};

