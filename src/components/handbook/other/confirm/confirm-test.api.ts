export const CONFIRM_API = {
  'properties': {
    'message': {'description': 'Message text', 'type': 'string'},
    'template': {'description': 'Message template', 'type': 'string'}
  }
};
