import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalService, TabsComponent, UI } from 'esanum-ui';
import { Language } from 'src/components/handbook/shared/code-highlight/enum';
import { SelectorType } from 'src/components/handbook/shared/component-api/enums';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { SHORTCUTS_API } from './shortcuts-test.api';

@Component({
  selector: 'app-shortcuts-test',
  templateUrl: './shortcuts-test.component.html',
  styleUrls: ['./shortcuts-test.component.scss']
})
export class ShortcutsTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  selectorType = SelectorType;
  actions = {anonym: 'Anonym', bound: 'Bound'};
  api = {
    shortcuts: SHORTCUTS_API
  };
  handbook = HANDBOOK;
  value: string;
  keys = Object.keys(UI.keyboard.key);
  modifiers = Object.keys(UI.keyboard.modifier);

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/core/directives/shortcuts';

  @ViewChild('tabs') tabs: TabsComponent;

  keyControl = this.fb.control(UI.keyboard.key.enter);
  modifierControl = this.fb.control(null);

  builder = this.fb.group({
    key: this.keyControl,
    modifier: this.modifierControl
  });

  form = this.fb.group({
    boundAction: [],
    anonymAction: []
  });

  constructor(private modalService: ModalService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

  anonymAction = () => {
    this.value = `${this.actions.anonym} action was called`;
  };

  boundAction() {
    this.value = `${this.actions.bound} action was called`;
  }
}
