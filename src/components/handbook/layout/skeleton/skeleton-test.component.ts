import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { SKELETON_API } from './skeleton-test.api';

export enum Sketch {
  User = 'user',
  VerticalPost = 'verticalPost',
  HorizontalPost = 'horizontalPost',
  Cards = 'cards'
}

@Component({
  selector: 'app-skeleton-test',
  templateUrl: './skeleton-test.component.html',
  styleUrls: ['./skeleton-test.component.scss']
})
export class SkeletonTestComponent implements OnInit {

  ui = UI;
  sketch = Sketch;
  localUi = LocalUI;
  language = Language;
  api = {
    skeleton: SKELETON_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/skeleton';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=1270%3A539';

  @ViewChild('tabs') tabs: TabsComponent;

  sketchControl = this.fb.control(Sketch.User);
  animationControl = this.fb.control(true);

  builder = this.fb.group({
    sketch: this.sketchControl,
    animation: this.animationControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
