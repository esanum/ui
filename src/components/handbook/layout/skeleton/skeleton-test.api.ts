export const SKELETON_API = {
  'properties': {
    'type': {
      'description': 'Skeleton type',
      'path': 'ui.skeleton.type',
      'default': 'text',
      'options': ['text', 'card', 'avatar', 'image']
    },
    'size': {'description': 'Avatar/image size', 'path': 'ui.size', 'default': 'normal', 'options': ['tiny', 'small', 'normal', 'large']},
    'width': {'description': 'Avatar/image width', 'type': 'string'},
    'height': {'description': 'Avatar/image height', 'type': 'string'},
    'lines': {'description': 'Count of text lines', 'type': 'number', 'default': '1'},
    'animated': {'description': 'Switch on/off animation', 'type': 'boolean', 'default': 'true'}
  }
};
