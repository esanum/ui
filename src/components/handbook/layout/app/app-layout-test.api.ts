export const APP_LAYOUT_API = {
  'content': {
    'header': {
      'selector': '<sn-app-header', 'description': 'Header of application'
    }
  }
};

export const APP_HEADER_API = {
  'content': {
    'headerLogoTemplate': {'selector': '#headerLogoTemplate', 'description': 'Logo template'},
    'contentTemplate': {'selector': '#headerContentTemplate', 'description': 'Header content template'},
    'headerUserbarTemplate': {'selector': '#headerUserbarTemplate', 'description': 'Userbar template'},
    'headerActionsTemplate': {'selector': '#headerActionsTemplate', 'description': 'Actions template'}
  }
};

export const APP_HEADER_ACTIONS_API = {
  'properties': {
    'gutter': {
      'description': 'Elements gutter',
      'path': 'ui.gutter',
      'default': 'tiny',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge']
    }
  }
};

export const APP_HEADER_ACTION_API = {
  'content': {
    'actionLabelTemplate': {
      'selector': '#actionLabelTemplate',
      'description': 'Action label template'
    },
    'actionContentTemplate': {'selector': '#actionContentTemplate', 'description': 'Action content template'}
  }
};

export const APP_CONTENT_API = {
  'properties': {
    'aside': {'description': 'Support padding for aside', 'type': 'AppAsideComponent'}
  }
};

export const APP_ASIDE_API = {
  'content': {
    'headerTemplate': {'selector': '#asideHeaderTemplate', 'description': 'Aside content template'},
    'footerTemplate': {'selector': '#asideFooterTemplate', 'description': 'Aside footer template'}
  }
};

export const APP_HEADER_USERBAR_API = {
  'methods': {'hide': {'description': 'Hide userbar dropdown'}},
  'content': {
    'userbarAvatarTemplate': {'selector': '#userbarAvatarTemplate', 'description': 'Userbar avatar template'},
    'userbarMenuTemplate': {'selector': '#userbarMenuTemplate', 'description': 'Userbar menu template'}
  }
};

export const APP_PAGE_HEADER_API = {
  'properties': {
    'icon': {'description': 'Icon for page header', 'type': 'string'},
    'title': {'description': 'Title for page header', 'type': 'string'},
    'teaser': {'description': 'Teaser for page header', 'type': 'string'}
  },
  'content': {
    'headerActionsTemplate': {'selector': '#headerActionsTemplate', 'description': 'Actions template'}
  }
};

