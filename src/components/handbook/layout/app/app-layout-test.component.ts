import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BreakpointService, TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import {
  APP_ASIDE_API,
  APP_CONTENT_API,
  APP_HEADER_ACTION_API,
  APP_HEADER_ACTIONS_API,
  APP_HEADER_API,
  APP_HEADER_USERBAR_API,
  APP_LAYOUT_API,
  APP_PAGE_HEADER_API
} from './app-layout-test.api';

@Component({
  selector: 'app-layout-test',
  templateUrl: './app-layout-test.component.html',
  styleUrls: ['./app-layout-test.component.scss']
})
export class AppLayoutTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  handbook = HANDBOOK;
  language = Language;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/app';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=1910%3A2611';

  api = {
    appLayout: APP_LAYOUT_API,
    appHeader: APP_HEADER_API,
    appActions: APP_HEADER_ACTIONS_API,
    appAction: APP_HEADER_ACTION_API,
    appContent: APP_CONTENT_API,
    appAside: APP_ASIDE_API,
    appHeaderUserbar: APP_HEADER_USERBAR_API,
    appPageHeader: APP_PAGE_HEADER_API
  };

  @ViewChild('tabs')
  tabs: TabsComponent;

  busynessControl = this.fb.control(true);
  headerControl = this.fb.control(true);
  logoControl = this.fb.control(true);
  menuControl = this.fb.control(true);
  actionControl = this.fb.control(true);
  userMenuControl = this.fb.control(true);
  asideControl = this.fb.control(true);
  footerControl = this.fb.control(true);
  pageHeaderControl = this.fb.control(true);
  breadcrumbsControl = this.fb.control(true);

  builder = this.fb.group({
    busyness: this.busynessControl,
    header: this.headerControl,
    logo: this.logoControl,
    menu: this.menuControl,
    action: this.actionControl,
    userMenu: this.userMenuControl,
    aside: this.asideControl,
    footer: this.footerControl,
    pageHeader: this.pageHeaderControl,
    breadcrumbs: this.breadcrumbsControl
  });

  constructor(private fb: FormBuilder,
              public breakpoint: BreakpointService,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
