import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { Language as HighlightLanguage } from '../../shared/code-highlight/enum';
import { STACK_API } from './stack-test.api';

@Component({
  selector: 'app-stack-test',
  templateUrl: './stack-test.component.html',
  styleUrls: ['./stack-test.component.scss']
})
export class StackTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  highlight = {language: HighlightLanguage};
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/tree/master/projects/esanum-ui/src/lib/layout/stack';

  api = {
    stack: STACK_API
  };

  @ViewChild('tabs') tabs: TabsComponent;

  typeControl = this.fb.control(null);
  gutterControl = this.fb.control(null);
  spacingControl = this.fb.control(null);
  paddingControl = this.fb.control(null);
  alignControl = this.fb.control(null);
  justifyControl = this.fb.control(null);
  wrapControl = this.fb.control(null);
  widthControl = this.fb.control(null);

  builder = this.fb.group({
    type: this.typeControl,
    gutter: this.gutterControl,
    spacing: this.spacingControl,
    padding: this.paddingControl,
    align: this.alignControl,
    justify: this.justifyControl,
    wrap: this.wrapControl,
    width: this.widthControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
