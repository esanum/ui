export const STACK_API = {
  'properties': {
    'width': {'description': 'Stack width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'orientation': {
      'description': 'Defined main axis of elements align',
      'path': 'ui.orientation',
      'default': 'vertical',
      'options': ['vertical', 'horizontal']
    },
    'gutter': {
      'description': 'Space between elements in main axis',
      'path': 'ui.gutter',
      'default': 'normal',
      'options': ['tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
    'spacing': {
      'description': 'Space between elements when wrapping on horizontal mode',
      'path': 'ui.gutter',
      'options': ['none', 'tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
    'padding': {
      'description': 'Padding for stack',
      'path': 'ui.gutter',
      'options': ['none', 'tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
    'align': {
      'description': 'Align of elements in main axis',
      'path': 'ui.align',
      'default': 'start',
      'options': ['start', 'center', 'end', 'baseline', 'stretch']
    },
    'justify': {
      'description': 'Align of elements in secondary axis',
      'path': 'ui.justify',
      'default': 'start',
      'options': ['start', 'center', 'end', 'between', 'around', 'evenly']
    },
    'wrap': {
      'description': 'Wrapping of elements in main axis',
      'path': 'ui.wrap',
      'default': 'nowrap',
      'options': ['nowrap', 'wrap', 'reverse']
    }
  }
};
