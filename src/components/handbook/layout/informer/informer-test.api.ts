export const INFORMER_API = {
  'properties': {
    'placement': {
      'description': 'Informer placement',
      'path': 'ui.placement',
      'default': 'fixed',
      'options': ['absolute', 'fixed']
    },
    'outer': {
      'description': 'Negative margin for informer',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
    'context': {
      'description': 'Informer parent container',
      'path': 'ui.context',
      'options': ['block', 'modal']
    },
    'icon': {
      'description': 'Icon of informer',
      'type': 'string',
      'default': 'UI.icons.information'
    },
    'backdrop': {'description': 'Backdrop of informer', 'type': 'ElementRef'},
    'ok': {
      'description': 'Click on button event',
      'type': 'EventEmitter'
    }
  },
  'content': {
    'informerMessage': {
      'selector': '<sn-informer-message [message]="Text"></sn-informer-message>',
      'description': 'Informer message'
    },
    'contentTemplate': {
      'selector': '#informerContentTemplate',
      'description': 'Informer content template'
    }
  }
};
