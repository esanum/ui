import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';
import { INFORMER_API } from './informer-test.api';

@Component({
  selector: 'app-informer-test',
  templateUrl: './informer-test.component.html',
  styleUrls: ['./informer-test.component.scss']
})
export class InformerTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    informer: INFORMER_API
  };
  handbook = HANDBOOK;
  errors: string[] = [];
  language = Language;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/informer';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=9331%3A146';

  state = {ok: false};

  @ViewChild('page', {read: ElementRef, static: false})
  backdrop: ElementRef<HTMLElement>;

  @ViewChild('tabs') tabs: TabsComponent;

  contentControl = this.fb.control(false);
  placementControl = this.fb.control(null);
  outerControl = this.fb.control(null);
  iconControl = this.fb.control(false);

  builder = this.fb.group({
    content: this.contentControl,
    placement: this.placementControl,
    outer: this.outerControl,
    icon: this.iconControl
  });

  testShowMessage = false;
  testMessage = 'Test Message';

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.contentControl.valueChanges.subscribe(value => {
      value ? this.iconControl.disable() : this.iconControl.enable();
    });
  }

  add() {
    this.errors.push('Authorization error');
  }
}
