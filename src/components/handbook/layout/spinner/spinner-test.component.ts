import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { SPINNER_API } from './spinner-test.api';

@Component({
  selector: 'app-spinner-test',
  templateUrl: './spinner-test.component.html',
  styleUrls: ['./spinner-test.component.scss']
})
export class SpinnerTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  api = {
    spinner: SPINNER_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/spinner';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=2194%3A2';

  @ViewChild('tabs') tabs: TabsComponent;


  sizeControl = this.fb.control(null);

  builder = this.fb.group({
    size: this.sizeControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }
}
