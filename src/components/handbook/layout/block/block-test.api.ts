export const BLOCK_API = {
  'properties': {
    'title': {'description': 'Title of block', 'type': 'string'},
    'padding': {
      'description': 'Padding for block',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge'],
      'default': 'normal'
    },
    'spacing': {
      'description': 'Spacing between header, body and footer',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge'],
      'default': 'normal'
    },
    'width': {'description': 'Block width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'state': {'description': 'State of block', 'path': 'ui.state', 'options': ['error', 'loading']},
    'features': {'description': 'Adapted block on mobile view', 'path': 'ui.feature', 'options': ['adapted']}
  },
  'methods': {
    'success': {'description': 'Show success animation'}
  },
  'content': {
    'blockHelpTemplate': {'selector': '#blockHelpTemplate', 'description': 'Block help template'},
    'blockHeaderTemplate': {'selector': '#blockHeaderTemplate', 'description': 'Block header template'},
    'blockFooterTemplate': {'selector': '#blockFooterTemplate', 'description': 'Block footer template'}
  }
};

