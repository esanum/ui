export const CONTAINER_API = {
  'properties': {
    'width': {
      'description': 'Container width',
      'path': 'ui.width',
      'default': 'default',
      'options': ['default', 'fluid']
    }
  }
};

export const ROW_API = {
  'properties': {
    'align': {
      'description': 'Vertical align columns',
      'path': 'ui.align',
      'default': 'start',
      'options': ['start', 'center', 'end', 'stretch']
    },
    'justify': {
      'description': 'Horizontal align of elements',
      'path': 'ui.justify',
      'default': 'start',
      'options': ['start', 'center', 'end', 'between', 'around', 'evenly']
    },
    'spacing': {
      'description': 'Spacing between columns while wrapping',
      'path': 'ui.gutter',
      'default': 'normal',
      'options': ['tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
    'gutter': {
      'description': 'Padding in column',
      'path': 'ui.gutter',
      'default': 'small',
      'options': ['none', 'tiny', 'small', 'normal', 'big', 'large', 'huge']
    }
  }
};

export const COLUMN_API = {
  'properties': {
    'mobile': {
      'description': 'Number of cells to occupy on screen < 768px',
      'type': 'number: 1...12',
      'default': '12'
    },
    'tablet': {'description': 'Number of cells to occupy on screen >= 768px', 'type': 'number: 1...12', 'default': '6'},
    'desktop': {'description': 'Number of cells to occupy on screen >= 992px', 'type': 'number: 1...12', 'default': '1'},
    'wide': {'description': 'Number of cells to occupy on screen >= 1200px', 'type': 'number: 1...12', 'default': '1'},
    'span': {'description': 'Number of cells to occupy for all breakpoints', 'type': 'number: 1...12', 'default': 'null'}
  }
};

