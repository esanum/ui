export const CARD_API = {
  'properties': {
    'title': {'description': 'Title of card', 'type': 'string'},
    'height': {'description': 'Height of card', 'path': 'ui.height', 'options': ['default', 'fluid'], 'default': 'default'},
    '__picture__': {'name': 'picture', 'description': 'Picture on card', 'type': 'string'},
    'orientation': {
      'description': 'Card orientation',
      'path': 'ui.orientation',
      'default': 'horizontal',
      'options': ['horizontal', 'vertical']
    },
    'icon': {'description': 'Icon indicator', 'type': 'string'},
    'state': {'description': 'State of card', 'path': 'ui.state', 'options': ['error', 'loading']},
    'padding': {
      'description': 'Padding for card',
      'path': 'ui.gutter',
      'default': 'normal',
      'options': ['none', 'tiny', 'small', 'normal', 'large', 'big', 'huge']
    },
    'width': {'description': 'Card width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'features': {'description': 'Сlickable card; Adapted card on mobile view', 'path': 'ui.feature', 'options': ['clickable', 'adapted']},
    'color': {'description': 'Card indicator color', 'type': 'string', 'default': 'purple'},
    'selected': {'description': 'Output event of click on card content', 'type': 'Event Emitter'},
    'spacing': {
      'description': 'Space between picture and content',
      'path': 'ui.gutter',
      'default': 'normal',
      'options': ['none', 'tiny', 'small', 'normal', 'big', 'large', 'huge']
    },
  },
  'content': {
    'dragTemplate': {'selector': '#cardDragTemplate', 'description': 'Card drag template'},
    'headerTemplate': {'selector': '#cardHeaderTemplate', 'description': 'Card header template'},
    'titleTemplate': {'selector': '#cardTitleTemplate', 'description': 'Card title template'},
    'footerTemplate': {'selector': '#cardFooterTemplate', 'description': 'Card footer template'},
    'cardActionsTemplate': {'selector': '#cardActionsTemplate', 'description': 'Card actions template'}
  }
};

