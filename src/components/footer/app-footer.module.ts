import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFooterComponent } from './app-footer.component';
import { EsanumUiModule } from 'esanum-ui';

@NgModule({
  imports: [
    CommonModule,
    EsanumUiModule
  ],
  declarations: [
    AppFooterComponent
  ],
  exports: [
    AppFooterComponent
  ]
})
export class AppFooterModule {
}
